import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-search-bar',
  templateUrl: 'search-bar.component.html',
  styleUrls: ['search-bar.component.scss'],
})
export class SearchBarComponent {

  @ViewChild('searchPhraseFrom')
  searchPhraseForm: NgForm;

  @Input()
  searchPhrase: string;

  @Output()
  searchEvent = new EventEmitter<string>();
  @Output()
  resetSearchEvent = new EventEmitter();

  submitSearch() {
    if(this.searchPhraseForm.valid) {
      this.searchEvent.emit(this.searchPhraseForm.value.search);
    }
  }

  resetSearch() {
    this.resetSearchEvent.emit();
  }
}
