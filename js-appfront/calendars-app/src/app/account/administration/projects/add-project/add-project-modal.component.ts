import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AdminService} from '../../../../redux/services/admin.service';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-project-modal',
  templateUrl: 'add-project-modal.component.html',
  styleUrls: ['add-project-modal.component.scss'],
})
export class AddProjectModalComponent {
  addProjectModalTitle = 'Add new project';
  errorMessage;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('addNewProjectForm')
  addNewProjectForm: NgForm;

  constructor(
    private adminService: AdminService,
    private router: Router,
  ) {
  }

  submitForm() {
    if (this.addNewProjectForm.valid) {
      this.adminService.saveNewProject(this.addNewProjectForm.value).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            const projectName = this.addNewProjectForm.value.name;
            this.errorMessage = null;
            this.addNewProjectForm.reset();
            this.modalClose.emit();
            this.router.navigate(['/admin/projects', projectName]);
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }
}
