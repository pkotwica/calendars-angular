package pkotwica.inz.calendars.initialdata;

import org.springframework.stereotype.Component;

@Component
public interface InitialData {
    void load();
}
