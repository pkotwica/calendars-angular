import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AdminService} from '../../../../../redux/services/admin.service';
import {Router} from '@angular/router';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../../../../app.redux';
import {GetAdminsProjectsInfo} from '../../../../../redux/actions/admin.actions';

@Component({
  selector: 'app-change-project-modal',
  templateUrl: 'change-project-modal.component.html',
  styleUrls: ['change-project-modal.component.scss'],
})
export class ChangeProjectModalComponent {
  editProjectModalTitle = 'Edit project';
  errorMessage;

  @Input()
  projectName: string;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('editProjectForm')
  editProjectForm: NgForm;

  constructor(
    private adminService: AdminService,
    private state: Store<State>,
    private router: Router,
  ) {
  }

  submitForm() {
    if (this.editProjectForm.valid) {
      this.adminService.editProject(this.editProjectForm.value, this.projectName).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            const projectName = this.editProjectForm.value.name;
            this.errorMessage = null;
            this.editProjectForm.reset();
            this.modalClose.emit();
            this.state.dispatch(new GetAdminsProjectsInfo(projectName));
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }

  onDelete() {
    this.adminService.removeProject(this.projectName).pipe(
      tap(submitInfo => {
        if (submitInfo.success) {
          this.errorMessage = null;
          this.editProjectForm.reset();
          this.modalClose.emit();
          this.router.navigate(['/admin/projects']);
        } else {
          this.errorMessage = submitInfo.messageInfo;
        }
      }),
      catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
    ).subscribe();
  }
}
