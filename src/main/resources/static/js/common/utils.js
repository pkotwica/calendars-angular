export const callPostToCalendarsApi = (url, body, messageBox, redirect = undefined) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
    })
        .then(res => res.json())
        .then(res => {

            messageBox.innerHTML = res.messageInfo;
            messageBox.classList.remove('d-none');
            if (res.success) {
                if (redirect) {
                    document.location.replace(redirect);
                } else {
                    document.location.reload();
                }
            }
        });
};

export const getSelectedOption = (selector) => {
    return selector.options[selector.selectedIndex].value;
};