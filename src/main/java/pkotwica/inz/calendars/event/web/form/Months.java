package pkotwica.inz.calendars.event.web.form;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum Months {
    JANUARY("January", 1),
    FEBRUARY("February", 2),
    MARCH("March", 3),
    APRIL("April", 4),
    MAY("May", 5),
    JUNE("June", 6),
    JULY("July", 7),
    AUGUST("August", 8),
    SEPTEMBER("September", 9),
    OCTOBER("October", 10),
    NOVEMBER("November", 11),
    DECEMBER("December", 12);

    private String name;
    private int number;

    Months(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public static Months getMonthByNumber(int number) {
        if (number < 1) {
            number = 12 - (Math.abs(number)%12);
        } else if (number > 12) {
            number = number%12;
            number = number==0 ? ++number : number;
        }

        int finalNumber = number;
        return Arrays.stream(Months.values()).filter(m -> m.getNumber() == finalNumber).findFirst().orElse(Months.JANUARY);
    }
}
