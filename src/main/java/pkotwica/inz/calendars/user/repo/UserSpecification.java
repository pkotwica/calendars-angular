package pkotwica.inz.calendars.user.repo;

import org.springframework.data.jpa.domain.Specification;
import pkotwica.inz.calendars.common.CommonSpecifications;
import pkotwica.inz.calendars.user.model.User;

import javax.persistence.criteria.Expression;
import java.util.Arrays;

public class UserSpecification {

    private static final String FIRST_NAME_FIELD = "firstName";
    private static final String LAST_NAME_FIELD = "lastName";
    private static final String EMAIL_FIELD = "email";
    private static final String JOB_FIELD = "jobPosition";
    private static final String LOGIN_FIELD = "login";

    public static Specification<User> searchInUserData(String phrase) {
        return searchInUserName(phrase).or(CommonSpecifications.searchInListOfFields(Arrays.asList(EMAIL_FIELD, JOB_FIELD, LOGIN_FIELD), phrase));
    }

    public static Specification<User> searchInUserName(String phrase) {
        String finalText = "%" + phrase + "%";

        return (root, query, builder) -> {
            Expression<String> userNameExpression = builder.concat(root.get(FIRST_NAME_FIELD), " ");
            userNameExpression = builder.concat(userNameExpression, root.get(LAST_NAME_FIELD));
            Expression<String> userNameReverseExpression = builder.concat(root.get(LAST_NAME_FIELD), " ");
            userNameReverseExpression = builder.concat(userNameReverseExpression, root.get(FIRST_NAME_FIELD));


            return builder.or(
                    builder.like(userNameExpression, finalText),
                    builder.like(userNameReverseExpression, finalText)
            );
        };
    }
}
