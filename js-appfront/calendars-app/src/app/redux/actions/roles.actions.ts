import {Action} from '@ngrx/store';
import {SystemRolesDataState} from '../interfaces/user.interfaces';

export enum RolesActionsTypes {
  GetRoles = '[Roles] get',
  GetRolesSuccess = '[Roles] get success',
  GetRolesFailure = '[Roles] get failure',
}

export class GetRoles implements Action {
  readonly type = RolesActionsTypes.GetRoles;
}

export class GetRolesSuccess implements Action {
  readonly type = RolesActionsTypes.GetRolesSuccess;

  constructor(public payload: SystemRolesDataState) {
  }
}

export class GetRolesFailure implements Action {
  readonly type = RolesActionsTypes.GetRolesFailure;
}

export type RolesActions = GetRoles | GetRolesSuccess | GetRolesFailure;
