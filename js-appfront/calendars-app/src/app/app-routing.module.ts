import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthComponent} from './auth/auth.component';
import {ErrorPageComponent} from './shared/error-page/error-page.component';


const routes: Routes = [
  {path: '', loadChildren: () => import('./account/account.module').then(c => c.AccountModule)},
  {path: 'login', component: AuthComponent},
  {path: 'not-found',
    children: [
      {path: '', component: ErrorPageComponent, data: {message: 'Page not found!'}},
      {path: 'month', component: ErrorPageComponent, data: {message: 'Invalid month passed!'}},
      {path: 'access', component: ErrorPageComponent, data: {message: 'Permission denied! You have not access to this page'}},
    ]
  },
  {path: '**', redirectTo: '/not-found'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
