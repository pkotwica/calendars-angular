import {Injectable} from '@angular/core';
import {selectCurrentUserRole} from '../selectors/user.selector';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {
  PageableAdministrateProjectsData,
  PageableAdministrateRoomsData,
  PageableAdministrateUsersData,
  PageableQueryInfo
} from '../interfaces/pageable.interface';
import {PageableService} from './pageable.service';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {StandardCalendarsApiResponse} from '../../shared/calendars.model';
import {ProjectInfoData} from '../interfaces/admin.interface';

@Injectable({
  providedIn: 'root',
})
export class AdminService {

  userRole$ = this.state.select(selectCurrentUserRole);
  adminRoleName = 'admin';
  moderatorRoleName = 'moderator';

  constructor(
    private state: Store<State>,
    private http: HttpClient,
    private pageableService: PageableService,
  ) {
  }

  isUserModerator(): Observable<boolean> {
    return this.userRole$.pipe(map((role: string) =>
      role && (
        role.toLowerCase().includes(this.adminRoleName) ||
        role.toLowerCase().includes(this.moderatorRoleName)
      )
    ));
  }

  getUserToAdministrate(queryParameters: PageableQueryInfo): Observable<PageableAdministrateUsersData> {
    return this.http.get<PageableAdministrateUsersData>(`${environment.calendarsApiUrl}/api/admin/users`,
      {params: queryParameters ? this.pageableService.createPageableRequestParams(queryParameters) : undefined}
    );
  }

  saveUserData(userDataForm, oldEmail: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/users/data`, this.parseUserDataFrom(userDataForm, oldEmail));
  }

  saveUserPass(userPassForm, userEmail: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/users/pass`, this.parseUserPassForm(userPassForm, userEmail));
  }

  getRoomsToAdministrate(queryParameters: PageableQueryInfo): Observable<PageableAdministrateRoomsData> {
    return this.http.get<PageableAdministrateRoomsData>(`${environment.calendarsApiUrl}/api/admin/rooms`,
      {params: queryParameters ? this.pageableService.createPageableRequestParams(queryParameters) : undefined}
    );
  }

  saveNewRoom(newRoomForm): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/rooms/add`, this.parseRoomForm(newRoomForm, ''));
  }

  saveRoomData(newRoomForm, roomOldName: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/rooms/edit`, this.parseRoomForm(newRoomForm, roomOldName));
  }

  removeRoom(roomName: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/rooms/remove`, roomName);
  }

  getProjectsToAdministrate(queryParameters: PageableQueryInfo): Observable<PageableAdministrateProjectsData> {
    return this.http.get<PageableAdministrateProjectsData>(`${environment.calendarsApiUrl}/api/admin/projects`,
      {params: queryParameters ? this.pageableService.createPageableRequestParams(queryParameters) : undefined}
    );
  }

  getProjectsInfo(projectName: string): Observable<ProjectInfoData> {
    return this.http.get<ProjectInfoData>(`${environment.calendarsApiUrl}/api/admin/projects/project/${projectName}`);
  }

  saveNewProject(newProjectForm: any): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/projects/add`, this.parseProjectForm(newProjectForm, ''));
  }

  editProject(editProjectForm: any, oldProjectName: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/projects/edit`, this.parseProjectForm(editProjectForm, oldProjectName));
  }

  removeProject(projectName): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/projects/remove`, this.parseProjectForm({name: projectName}, ''));
  }

  removeUserFromProject(userEmail: string, projectName: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/projects/remove/user`, {projectName, userEmail});
  }

  addUserToProject(userEmail: string, projectName: string): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/admin/projects/add/user`, {projectName, userEmail});
  }

  private parseUserDataFrom(userDataForm, oldUserEmail) {
    return {
      oldUserEmail,
      firstName: userDataForm.firstName,
      lastName: userDataForm.lastName,
      login: userDataForm.login,
      jobPosition: userDataForm.jobPosition,
      email: userDataForm.email,
      role: userDataForm.role,
    };
  }

  private parseUserPassForm(userPassForm, email: string) {
    return {
      userEmail: email,
      newPassword: userPassForm.pass,
      repeatPassword: userPassForm.passRepeat,
    };
  }

  private parseRoomForm(roomForm, roomName: string) {
    return {
      oldName: roomName,
      roomName: roomForm.name,
      roomNumber: roomForm.number,
    };
  }

  private parseProjectForm(projectForm, projectName: string) {
    return {
      projectName: projectForm.name,
      oldProjectName: projectName,
    };
  }
}
