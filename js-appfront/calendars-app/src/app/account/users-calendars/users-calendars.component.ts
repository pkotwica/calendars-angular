import {Component, OnInit} from '@angular/core';
import {selectCalendarState} from '../../redux/selectors/calendar.selector';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetUsersCalendarData} from '../../redux/actions/calendar.actions';
import {GetUsersEmailsData} from '../../redux/actions/user.actions';
import {UsersEmailsDataState} from '../../redux/interfaces/user.interfaces';
import {SelectorDataModel, SelectorOptionModel} from '../../shared/selector/selector-data.model';
import {selectCurrentUserEmail, selectUsersEmailsData} from '../../redux/selectors/user.selector';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-users-calendars',
  templateUrl: 'users-calendars.component.html',
  styleUrls: ['users-calendars.component.scss']
})
export class UsersCalendarsComponent implements OnInit {
  usersEmailsData$ = this.state.select(selectUsersEmailsData);
  calendarData$ = this.state.select(selectCalendarState);
  userData$ = this.state.select(selectCurrentUserEmail);
  monthNumber: number;
  year: number;
  userEmail: string;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.monthNumber = this.route.snapshot.params['monthNumber'];
    this.year = this.route.snapshot.params['year'];
    this.userEmail = this.route.snapshot.queryParams['email'];
    this.getUsersCalendarData();
    this.getListOfUsers();

    this.route.params.subscribe((params: Params) => {
      this.monthNumber = params['monthNumber'];
      this.year = params['year'];
      this.getUsersCalendarData();
    });

    this.route.queryParams.subscribe((params: Params) => {
      this.userEmail = params['email'];
      this.getUsersCalendarData();
    });
  }

  onChangeMonth(event) {
    this.router.navigate(['/users-calendars', event.monthNumber, event.year], {queryParams: {email: this.userEmail}});
  }

  getUsersCalendarData() {
    if (this.monthNumber > 12 || this.monthNumber < 1) {
      this.router.navigate(['/not-found/month']);
    } else {
      this.dispatchUsersCalendarData();
    }
  }

  dispatchUsersCalendarData() {
    if (this.userEmail) {
      this.state.dispatch(new GetUsersCalendarData({
        monthNumber: this.monthNumber,
        year: this.year,
        userEmail: this.userEmail
      }));
    }
  }

  private getListOfUsers() {
    this.state.dispatch(new GetUsersEmailsData());
  }


  parseSelectorData(usersEmails: UsersEmailsDataState): SelectorDataModel {
    const usersEmailsSelect: SelectorOptionModel[] = [];

    if (usersEmails.data) {
      usersEmails.data.forEach(user => usersEmailsSelect.push({value: user.email, name: user.name}));
    }

    return {values: usersEmailsSelect}
  }

  onChoseUser(userEmail: string) {
    let urlPath = '';
    if (this.monthNumber) {
      urlPath = `/${this.monthNumber}${this.year ? '/' + this.year : ''}`
    }

    this.router.navigate(
      ['/users-calendars' + urlPath],
      {queryParams: {email: userEmail}}
    );
  }

  isMineCalendar(): boolean {
    let isMine = false;

    this.userData$.pipe(take(1)).subscribe(currentEmail => {
      isMine = currentEmail === this.userEmail;
    });

    return isMine;
  };
}
