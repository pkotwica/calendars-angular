package pkotwica.inz.calendars.project.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.project.web.form.ProjectUsersList;

import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@Controller
public class ProjectController {

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    private static class ProjectsList {
        private List<String> projects;
    }

    private ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping(PROTECTED_API_PREFIX + "/projects")
    @ResponseBody
    public ProjectsList getProjectsForLoggedUser() {
        return new ProjectsList(projectService.getProjectsByLoggedUser());
    }

    @GetMapping(PROTECTED_API_PREFIX + "/projects/users")
    @ResponseBody
    public ProjectUsersList getProjectsForLoggedUser(@RequestParam String project) {
        return ProjectUsersList.of(projectService.getUsersByProject(project));
    }
}
