import {Component, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from './auth.service';
import {Store} from '@ngrx/store';
import {State} from '../app.redux';
import {Auth} from '../redux/actions/auth.actions';
import {selectAuthStatus} from '../redux/selectors/auth.selector';

@Component({
  selector: 'app-auth',
  templateUrl: 'auth.component.html',
  styleUrls: ['auth.component.scss'],
})
export class AuthComponent {

  @ViewChild('loginForm')
  loginForm: NgForm;

  errorStatus$ = this.state.select(selectAuthStatus);

  constructor(
    private authService: AuthService,
    private state: Store<State>,
  ) {
  }

  onLogin() {
    const login = this.loginForm.value.email;
    const pass = this.loginForm.value.pass;
    this.state.dispatch(new Auth({username: login, password: pass}));
  }
}
