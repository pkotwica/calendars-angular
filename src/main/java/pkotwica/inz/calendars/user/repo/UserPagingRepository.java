package pkotwica.inz.calendars.user.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import pkotwica.inz.calendars.user.model.User;

import java.util.List;

public interface UserPagingRepository extends PagingAndSortingRepository<User, Long> {

    Page<User> findAll(Specification<User> spec, Pageable page);
}
