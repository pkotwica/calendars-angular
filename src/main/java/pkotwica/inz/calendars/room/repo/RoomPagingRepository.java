package pkotwica.inz.calendars.room.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.room.model.Room;

@Repository
public interface RoomPagingRepository extends PagingAndSortingRepository<Room, Long> {

    Page<Room> findAll(Specification<Room> spec, Pageable page);
}
