package pkotwica.inz.calendars.registration.web;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pkotwica.inz.calendars.registration.factory.RegistrationResponseFactory;
import pkotwica.inz.calendars.registration.form.RegisterForm;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.Collection;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@Controller
@RequestMapping()
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class RegistrationController {
    private final UserService userService;

    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("register")
    public String getRegisterPage(@ModelAttribute RegisterForm registerForm) {
        return "admin/register";
    }

    @GetMapping("register/users")
    public String getUsersPage(@ModelAttribute RegisterForm registerForm, Model model) {
        model.addAttribute("users", userService.getRegisteredUsers());
        return "admin/users";
    }

    @PostMapping("register")
    public String registerUser(@Valid @ModelAttribute RegisterForm registerForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error",true);
        } else if (passwordIsNotAccepted(registerForm)) {
            model.addAttribute("errorPassword",true);
        } else {
            Collection<String> saveErr  = userService.addNewUser(registerForm);
            if (saveErr.isEmpty()) {
                model.addAttribute("success", true);
            } else {
                for (String err : saveErr) {
                    model.addAttribute(err, true);
                }
            }
        }

        return "admin/register";
    }

    @PostMapping(PROTECTED_API_PREFIX + "/register")
    @ResponseBody
    public CalendarsApiResponse registerUserApi(@Valid @RequestBody RegisterForm registerForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return CalendarsApiResponseFactory.fillAllError();
        } else if (passwordIsNotAccepted(registerForm)) {
            return RegistrationResponseFactory.passByIsSuccess(false);
        } else {
            return RegistrationResponseFactory.byErrList(userService.addNewUser(registerForm));
        }
    }

    private boolean passwordIsNotAccepted(RegisterForm registerForm) {
        return !registerForm.getPassword().equals(registerForm.getPasswordRep());
    }

    @ModelAttribute(name = "userRoles")
    public Collection<String> getAllUserRoles() {
        return userService.getAllUserRoles();
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
