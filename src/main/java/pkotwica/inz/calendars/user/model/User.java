package pkotwica.inz.calendars.user.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pkotwica.inz.calendars.authorization.model.AuthToken;
import pkotwica.inz.calendars.event.model.Event;
import pkotwica.inz.calendars.project.model.Project;
import pkotwica.inz.calendars.registration.form.RegisterForm;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@Entity(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(unique = true)
    private String login;
    private String pass;
    private String firstName;
    private String lastName;
    private String jobPosition;
    @Column(unique = true)
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", foreignKey = @ForeignKey(name = "role_id"))
    private Role role;

    @ManyToMany(mappedBy = "users")
    private Set<Project> projects;

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private Set<Event> events;

    @OneToOne(orphanRemoval = true)
    private AuthToken token;


    public static User of(RegisterForm registerForm, Role role, String pass) {
        return new User(registerForm, role, pass);
    }

    private User(RegisterForm registerForm, Role role, String pass) {
        this.login = registerForm.getLogin();
        this.pass = pass;
        this.firstName = registerForm.getFirstName();
        this.lastName = registerForm.getLastName();
        this.jobPosition = registerForm.getJobPosition();
        this.email = registerForm.getEmail();
        this.role = role;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(role);
    }

    @Override
    public String getPassword() {
        return this.pass;
    }

    @Override
    public String getUsername() {
        return this.login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
