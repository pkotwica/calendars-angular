import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {UserDataState} from '../../../../redux/interfaces/user.interfaces';
import {NgForm} from '@angular/forms';
import {AdminService} from '../../../../redux/services/admin.service';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-change-user-pass-modal',
  templateUrl: 'change-user-pass-modal.component.html',
})
export class ChangeUserPassModalComponent {
  changeUserPassModalTitle = 'Change User Password';
  errorMessage;

  @Input()
  user: UserDataState;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('changeUserPassForm')
  changeUserPassForm: NgForm;

  matchRepeatPass: boolean = false;
  repeatPassInputBlur: boolean = false;

  constructor(
    private adminService: AdminService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.changeUserPassForm.valid) {
      this.adminService.saveUserPass(this.changeUserPassForm.value, this.user.email).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            this.errorMessage = null;
            this.changeUserPassForm.reset();
            this.modalClose.emit();
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }

  checkMatchPass() {
    this.matchRepeatPass = this.changeUserPassForm.value.pass === this.changeUserPassForm.value.passRepeat;
  }
}
