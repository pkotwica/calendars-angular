package pkotwica.inz.calendars.event.model;

import lombok.*;
import pkotwica.inz.calendars.event.web.form.SaveEventForm;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.user.model.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "events")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String label;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @ManyToOne
    @JoinColumn(name = "event_type_id", foreignKey = @ForeignKey(name = "event_type_id"))
    private EventType type;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "user_id"))
    private User user;

    @ManyToOne
    @JoinColumn(name = "room_id", foreignKey = @ForeignKey(name = "room_id"))
    private Room room;

    public static Event of(Event event, SaveEventForm saveEventForm) {
        event.setLabel(saveEventForm.getEventLabel());
        event.setStartDate(LocalDateTime.of(saveEventForm.getYear(), saveEventForm.getMonth(), saveEventForm.getDay(), saveEventForm.getStartHour(), saveEventForm.getStartMinute()));
        event.setEndDate(LocalDateTime.of(saveEventForm.getYear(), saveEventForm.getMonth(), saveEventForm.getDay(), saveEventForm.getEndHour(), saveEventForm.getEndMinute()));
        return event;
    }
}
