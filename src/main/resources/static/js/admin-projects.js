import {
    callPostToCalendarsApi,
    getSelectedOption,
} from './common/utils';

export const bindAdminProjects = () => {
    const adminProjects = document.getElementById('js-admin-projects');
    if (adminProjects) {
        bindSaveNewProjectButton();
        bindRemoveProjectButton();
        bindAddUserToProjectButton();
        bindRemoveUserFromProjectButtons();
    }
};

const bindSaveNewProjectButton = () => {
    const saveNewProjectButton = document.getElementById('createNewProjectModalButton');

    if (saveNewProjectButton) {
        saveNewProjectButton.addEventListener('click', function () {
            const body = {
                projectName: document.getElementById('addNewProjectInput').value,
            };
            callPostToCalendarsApi('/admin-projects/add', body, document.getElementById('addNewProjectMessage'));
        });
    }
};

const bindRemoveProjectButton = () => {
    const removeProjectButton = document.getElementById('removeProjectModalButton');

    if (removeProjectButton) {
        removeProjectButton.addEventListener('click', function () {
            const body = {
                projectName: removeProjectButton.dataset.project,
            };
            callPostToCalendarsApi('/admin-projects/remove', body, document.getElementById('removeProjectModalMessage'), '/admin-projects');
        });
    }
};

const bindAddUserToProjectButton = () => {
    const saveNewProjectButton = document.getElementById('addUserProjectModalButton');
    const userSelector = document.getElementById('userSelectProject');

    if (saveNewProjectButton) {
        saveNewProjectButton.addEventListener('click', function () {
            const body = {
                projectName: saveNewProjectButton.dataset.project,
                userEmail: getSelectedOption(userSelector),
            };
            callPostToCalendarsApi('/admin-projects/add/user', body, document.getElementById('addUserProjectMessage'));
        });
    }
};

const bindRemoveUserFromProjectButtons = () => {
    const removeUserFromProjectButtons = document.getElementsByClassName('js-remove-user-project');

    for (let button of removeUserFromProjectButtons) {
        button.addEventListener('click', function () {
            const body = {
                projectName: button.dataset.project,
                userEmail: button.dataset.email,
            };
            callPostToCalendarsApi('/admin-projects/remove/user', body, document.getElementById('alertAdminProjectMessage'));
        })
    }
};