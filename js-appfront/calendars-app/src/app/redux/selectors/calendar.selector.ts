import {State} from '../../app.redux';
import {createSelector} from '@ngrx/store';
import {
  CalendarDataState,
  EventsTypesDataState,
  OwnedProjectsDataState,
  RoomsDataState
} from '../interfaces/calendar.interfaces';
import {UserDataState} from '../interfaces/user.interfaces';

export const selectCalendar = (globalState: State): CalendarDataState => globalState.calendarData;

export const selectCalendarState = createSelector(selectCalendar, (state: CalendarDataState): CalendarDataState => state);

export const selectEventsTypes = (globalState: State): EventsTypesDataState => globalState.eventsTypes;

export const selectEventsTypesState = createSelector(selectEventsTypes, (state: EventsTypesDataState): String[] => state.eventsTypes);

export const selectRooms = (globalState: State): RoomsDataState => globalState.roomsData;

export const selectRoomsState = createSelector(selectRooms, (state: RoomsDataState): RoomsDataState => state);

export const selectProjects = (globalState: State): OwnedProjectsDataState => globalState.ownedProjects;

export const selectOwnedProjects = createSelector(selectProjects, (state: OwnedProjectsDataState): OwnedProjectsDataState => state);
export const selectProjectUsers = createSelector(selectProjects, (state: OwnedProjectsDataState): UserDataState[] => state.users);
