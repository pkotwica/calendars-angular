import {Action} from '@ngrx/store';
import {
  PageableAdministrateProjectsData,
  PageableAdministrateRoomsData,
  PageableAdministrateUsersData,
  PageableQueryInfo
} from '../interfaces/pageable.interface';
import {ProjectInfoData} from '../interfaces/admin.interface';

export enum AdminActionsTypes {
  GetUsers = '[Admin] get users',
  GetUsersSuccess = '[Admin] get users success',
  GetUsersFailure = '[Admin] get users failure',

  GetRooms = '[Admin] get rooms',
  GetRoomsSuccess = '[Admin] get rooms success',
  GetRoomsFailure = '[Admin] get rooms failure',

  GetAdminsProjects = '[Admin] get projects',
  GetAdminsProjectsSuccess = '[Admin] get rooms projects success',
  GetAdminsProjectsFailure = '[Admin] get rooms projects failure',

  GetAdminsProjectsInfo = '[Admin] get projects info',
  GetAdminsProjectsInfoSuccess = '[Admin] get rooms projects info success',
  GetAdminsProjectsInfoFailure = '[Admin] get rooms projects info failure',
}

export class GetUsers implements Action {
  readonly type = AdminActionsTypes.GetUsers;

  constructor(public payload: PageableQueryInfo) {
  }
}

export class GetUsersSuccess implements Action {
  readonly type = AdminActionsTypes.GetUsersSuccess;

  constructor(public payload: PageableAdministrateUsersData) {
  }
}

export class GetUsersFailure implements Action {
  readonly type = AdminActionsTypes.GetUsersFailure;
}

export class GetAdminRooms implements Action {
  readonly type = AdminActionsTypes.GetRooms;

  constructor(public payload: PageableQueryInfo) {
  }
}

export class GetAdminRoomsSuccess implements Action {
  readonly type = AdminActionsTypes.GetRoomsSuccess;

  constructor(public payload: PageableAdministrateRoomsData) {
  }
}

export class GetAdminRoomsFailure implements Action {
  readonly type = AdminActionsTypes.GetRoomsFailure;
}

export class GetAdminsProjects implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjects;

  constructor(public payload: PageableQueryInfo) {
  }
}

export class GetAdminsProjectsSuccess implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjectsSuccess;

  constructor(public payload: PageableAdministrateProjectsData) {
  }
}

export class GetAdminsProjectsFailure implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjectsFailure;
}

export class GetAdminsProjectsInfo implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjectsInfo;

  constructor(public payload: string) {
  }
}

export class GetAdminsProjectsInfoSuccess implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjectsInfoSuccess;

  constructor(public payload: ProjectInfoData) {
  }
}

export class GetAdminsProjectsInfoFailure implements Action {
  readonly type = AdminActionsTypes.GetAdminsProjectsInfoFailure;
}

export type AdminActions =
  GetUsers |
  GetUsersSuccess |
  GetUsersFailure |
  GetAdminRooms |
  GetAdminRoomsSuccess |
  GetAdminRoomsFailure |
  GetAdminsProjects |
  GetAdminsProjectsSuccess |
  GetAdminsProjectsFailure |
  GetAdminsProjectsInfo |
  GetAdminsProjectsInfoSuccess |
  GetAdminsProjectsInfoFailure;
