import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AccountComponent} from './account.component';
import {AuthGuard} from '../auth/auth.guard';
import {MyAccountComponent} from './my-account/my-account.component';
import {MyCalendarComponent} from './my-calendar/my-calendar.component';
import {UsersCalendarsComponent} from './users-calendars/users-calendars.component';
import {ProjectsCalendarsComponent} from './projects-calendars/projects-calendars.component';
import {RoomsCalendarsComponent} from './rooms-calendars/rooms-calendars.component';

const accountRoutes: Routes = [
  {
    path: '', component: AccountComponent, canActivate: [AuthGuard], children: [
      {path: '', component: MyAccountComponent},
      {path: 'my-calendar',
        children: [
          {path: '', component: MyCalendarComponent},
          {path: ':monthNumber', component: MyCalendarComponent},
          {path: ':monthNumber/:year', component: MyCalendarComponent},
        ]
      },
      {path: 'users-calendars',
        children: [
          {path: '', component: UsersCalendarsComponent},
          {path: ':monthNumber', component: UsersCalendarsComponent},
          {path: ':monthNumber/:year', component: UsersCalendarsComponent},
        ]
      },
      {path: 'projects-calendars',
        children: [
          {path: '', component: ProjectsCalendarsComponent},
          {path: ':monthNumber', component: ProjectsCalendarsComponent},
          {path: ':monthNumber/:year', component: ProjectsCalendarsComponent},
        ]
      },
      {path: 'rooms-calendars',
        children: [
          {path: '', component: RoomsCalendarsComponent},
          {path: ':monthNumber', component: RoomsCalendarsComponent},
          {path: ':monthNumber/:year', component: RoomsCalendarsComponent},
        ]
      },
      {path: 'admin', loadChildren: () => import('./administration/admin.module').then(c => c.AdminModule)},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(accountRoutes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {

}
