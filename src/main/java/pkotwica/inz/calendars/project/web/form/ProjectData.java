package pkotwica.inz.calendars.project.web.form;

import lombok.Builder;
import lombok.Getter;
import pkotwica.inz.calendars.project.model.Project;

@Getter
@Builder
public class ProjectData {
    private String name;

    public static ProjectData of(Project project) {
        return new ProjectData(project.getName());
    }
}
