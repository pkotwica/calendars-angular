package pkotwica.inz.calendars.registration.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class EditUserDataForm {
    @NotNull
    @NotEmpty
    private String oldUserEmail;

    @NotNull
    @NotEmpty
    private String firstName;
    @NotNull
    @NotEmpty
    private String lastName;
    @NotNull
    @NotEmpty
    private String login;
    @NotNull
    @NotEmpty
    private String jobPosition;
    @NotNull
    @NotEmpty
    private String email;
    @NotNull
    @NotEmpty
    private String role;
}
