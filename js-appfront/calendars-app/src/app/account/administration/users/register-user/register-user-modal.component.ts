import {Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../../../redux/services/user.service';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {selectSystemRolesData} from '../../../../redux/selectors/user.selector';

@Component({
  selector: 'app-register-user-modal',
  templateUrl: 'register-user-modal.component.html',
})
export class RegisterUserModalComponent {
  registerUserModalTitle = 'Register New User';
  errorMessage;
  matchRepeatPass: boolean = false;
  isRepeatInputBlur: boolean = false;
  rolesData$ = this.state.select(selectSystemRolesData);

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('registerUserForm')
  registerUserForm: NgForm;

  constructor(
    private userService: UserService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.registerUserForm.valid && this.matchRepeatPass) {
      this.userService.saveNewUser(this.registerUserForm.value).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            this.errorMessage = null;
            this.registerUserForm.reset();
            this.modalClose.emit();
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }

  checkMatchPass() {
    this.matchRepeatPass = this.registerUserForm.value.pass === this.registerUserForm.value.passRepetition;
  }
}
