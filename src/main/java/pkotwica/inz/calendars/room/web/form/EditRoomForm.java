package pkotwica.inz.calendars.room.web.form;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class EditRoomForm {
    private String oldName;
    @NotEmpty
    @NotNull
    private String roomName;
    @NotEmpty
    @NotNull
    private String roomNumber;
}
