package pkotwica.inz.calendars.room.web;

import org.apache.logging.log4j.util.Strings;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.room.web.factory.EditRoomResponseFactory;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

@Controller
@RequestMapping("admin-rooms")
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class AdminRoomController {
    private final UserService userService;
    private final RoomService roomService;

    public AdminRoomController(UserService userService, RoomService roomService) {
        this.userService = userService;
        this.roomService = roomService;
    }

    @GetMapping
    public String getAdminRooms(Model model) {
        model.addAttribute("rooms", roomService.getAllRoomsData());
        return "admin/admin-rooms";
    }

    @PostMapping("add")
    @ResponseBody
    public CalendarsApiResponse addNewRoom(@Valid @RequestBody EditRoomForm editRoomForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.addByIsSuccess(roomService.addNewRoom(editRoomForm));
    }

    @PostMapping("edit")
    @ResponseBody
    public CalendarsApiResponse editRoom(@Valid @RequestBody EditRoomForm editRoomForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.editByIsSuccess(roomService.editRoom(editRoomForm));
    }

    @PostMapping("remove")
    @ResponseBody
    public CalendarsApiResponse removeProject(@RequestParam String room) {
        if (Strings.isEmpty(room)) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.removeByIsSuccess(roomService.removeRoom(room));
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
