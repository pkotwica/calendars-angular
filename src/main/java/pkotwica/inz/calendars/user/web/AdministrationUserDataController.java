package pkotwica.inz.calendars.user.web;

import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.administration.user.AdminUserFacade;
import pkotwica.inz.calendars.registration.factory.EditUserDataResponseFactory;
import pkotwica.inz.calendars.registration.form.EditUserDataForm;
import pkotwica.inz.calendars.registration.form.EditUserPasswordForm;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.web.common.AbstractPageableController;
import pkotwica.inz.calendars.web.common.PageableResponse;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@RestController
@RequestMapping(PROTECTED_API_PREFIX + "/admin/users")
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class AdministrationUserDataController extends AbstractPageableController {

    private AdminUserFacade adminUserFacade;

    public AdministrationUserDataController(AdminUserFacade adminUserFacade) {
        this.adminUserFacade = adminUserFacade;
    }

    @GetMapping
    public PageableResponse<List<UserData>> getPageableUsers(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                                                             @RequestParam(value = "size", required = false, defaultValue = "10") int pageSize,
                                                             @RequestParam(value = "search", required = false) String searchPhrase) {

        Page<UserData> userDataPage = adminUserFacade.findUsersToAdministrate(--pageNumber, pageSize, searchPhrase);
        return pageableResponseOf(userDataPage);
    }

    @PostMapping("data")
    public CalendarsApiResponse updateUserData(@Valid @RequestBody EditUserDataForm editUserDataForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditUserDataResponseFactory.fillAllError();
        }
        return EditUserDataResponseFactory.ediByIsSuccess(adminUserFacade.editUserData(editUserDataForm));
    }

    @PostMapping("pass")
    public CalendarsApiResponse updateUserPass(@Valid @RequestBody EditUserPasswordForm editUserPasswordForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditUserDataResponseFactory.fillAllError();
        }
        return EditUserDataResponseFactory.ediByIsSuccess(adminUserFacade.editUserPassword(editUserPasswordForm));
    }
}
