import {
  PageableAdministrateProjectsData,
  PageableAdministrateRoomsData,
  PageableAdministrateUsersData
} from './pageable.interface';
import {UserDataState} from './user.interfaces';
import {Data} from './common.interfaces';

export interface AdminState {
  usersToAdministrate?: PageableAdministrateUsersData;
  roomsToAdministrate?: PageableAdministrateRoomsData;
  projectAdministration?: ProjectsAdministration;
}

export interface ProjectsAdministration {
  projectsToAdministrate?: PageableAdministrateProjectsData;
  projectInfoData?: ProjectInfoData;
}

export interface ProjectInfoData extends Data{
  name?: string;
  members?: UserDataState[];
  notMembers?: UserDataState[];
}

export interface ProjectInfo {
  name: string;
}
