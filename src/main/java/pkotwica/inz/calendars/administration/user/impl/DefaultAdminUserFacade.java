package pkotwica.inz.calendars.administration.user.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.administration.user.AdminUserFacade;
import pkotwica.inz.calendars.registration.form.EditUserDataForm;
import pkotwica.inz.calendars.registration.form.EditUserPasswordForm;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

@Service
public class DefaultAdminUserFacade implements AdminUserFacade {

    private UserService userService;

    public DefaultAdminUserFacade(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Page<UserData> findUsersToAdministrate(int pageNumber, int pageSize, String searchPhrase) {
        Pageable page = PageRequest.of(pageNumber, pageSize);

        return searchPhrase != null && !searchPhrase.isEmpty() ?
                userService.getRegisteredUsers(page, searchPhrase) :
                userService.getRegisteredUsers(page);
    }

    @Override
    public boolean editUserData(EditUserDataForm editUserDataForm) {
        return userService.editUserData(editUserDataForm);
    }

    @Override
    public boolean editUserPassword(EditUserPasswordForm editUserPasswordForm) {
        return userService.editUserPassword(editUserPasswordForm);
    }
}
