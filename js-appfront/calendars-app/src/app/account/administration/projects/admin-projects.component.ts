import {Component, OnInit} from '@angular/core';
import {selectProjectsToAdminState} from '../../../redux/selectors/admin.selector';
import {Store} from '@ngrx/store';
import {State} from '../../../app.redux';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ProjectInfo} from '../../../redux/interfaces/admin.interface';
import {GetAdminsProjects} from '../../../redux/actions/admin.actions';

@Component({
  selector: 'app-admin-projects',
  templateUrl: 'admin-projects.component.html',
  styleUrls: ['admin-projects.component.scss'],
})
export class AdminProjectsComponent implements OnInit {
  pageNumberParamName: string = 'page';
  searchPhraseParamName: string = 'search';
  pageNumber: number;
  searchPhrase: string;

  projectsToAdministrate$ = this.state.select(selectProjectsToAdminState);

  isNewProjectModal: boolean = false;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.pageNumber = this.route.snapshot.queryParams[this.pageNumberParamName];
    this.searchPhrase = this.route.snapshot.queryParams[this.searchPhraseParamName];
    this.state.dispatch(new GetAdminsProjects({page: this.pageNumber, searchPhrase: this.searchPhrase}));

    this.route.queryParams.subscribe((params: Params) => {
      this.pageNumber = params[this.pageNumberParamName];
      this.searchPhrase = params[this.searchPhraseParamName];
      this.state.dispatch(new GetAdminsProjects({page: this.pageNumber, searchPhrase: this.searchPhrase}));
    });
  }

  administrateProject(project: ProjectInfo) {
    this.router.navigate(['/admin/projects', project.name]);
  }

  onPageSelected(pageNumber: number) {
    this.router.navigate(['/admin/projects'], {
      queryParams: {[this.pageNumberParamName]: pageNumber, [this.searchPhraseParamName]: this.searchPhrase}
    })
  }

  onSearchPhrase(searchPhrase: string) {
    this.router.navigate(['/admin/projects'], {
      queryParams: {[this.searchPhraseParamName]: searchPhrase}
    })
  }

  onResetSearch() {
    this.router.navigate(['/admin/projects'])
  }
}
