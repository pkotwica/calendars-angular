package pkotwica.inz.calendars.web.common;

import org.springframework.data.domain.Page;

import java.util.List;

public abstract class AbstractPageableController {

    protected <E> PageableResponse<List<E>> pageableResponseOf(Page<E> dataPage) {
        return new PageableResponse<>(dataPage.getNumber() + 1, dataPage.getTotalPages(), dataPage.getContent());
    }
}
