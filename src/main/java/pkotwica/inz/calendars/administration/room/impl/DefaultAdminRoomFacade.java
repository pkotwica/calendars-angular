package pkotwica.inz.calendars.administration.room.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.administration.room.AdminRoomFacade;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.room.web.form.RoomData;

@Service
public class DefaultAdminRoomFacade implements AdminRoomFacade {

    private RoomService roomService;

    public DefaultAdminRoomFacade(RoomService roomService) {
        this.roomService = roomService;
    }

    @Override
    public Page<RoomData> findRoomsToAdministrate(int pageNumber, int pageSize, String searchPhrase) {
        Pageable page = PageRequest.of(pageNumber, pageSize);

        return searchPhrase != null && !searchPhrase.isEmpty() ?
                roomService.getAllRoomsData(page, searchPhrase) :
                roomService.getAllRoomsData(page);
    }

    @Override
    public boolean addNewRoom(EditRoomForm editRoomForm) {
        return roomService.addNewRoom(editRoomForm);
    }

    @Override
    public boolean editRoom(EditRoomForm editRoomForm) {
        return roomService.editRoom(editRoomForm);
    }

    @Override
    public boolean removeRoom(String roomName) {
        return roomService.removeRoom(roomName);
    }
}
