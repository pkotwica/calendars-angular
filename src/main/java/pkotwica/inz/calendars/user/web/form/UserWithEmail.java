package pkotwica.inz.calendars.user.web.form;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserWithEmail {
    String name;
    String email;
}
