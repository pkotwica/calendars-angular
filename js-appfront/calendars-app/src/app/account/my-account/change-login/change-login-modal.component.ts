import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../../redux/services/user.service';
import {Store} from '@ngrx/store';
import {State} from '../../../app.redux';
import {ChangeUserLoginSuccess} from '../../../redux/actions/user.actions';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-change-login-modal',
  templateUrl: 'change-login-modal.component.html',
})
export class ChangeLoginModalComponent {
  changeLoginModalTitle = 'Change login';
  errorMessage;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('changeLoginForm')
  changeLoginForm: NgForm;

  constructor(
    private userService: UserService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.changeLoginForm.valid) {
      this.userService.changeUserLogin(this.changeLoginForm.value.newLogin).pipe(
        tap( submitInfo => {
          if (submitInfo.success) {
            this.state.dispatch(new ChangeUserLoginSuccess());
            this.errorMessage = null;
            this.changeLoginForm.reset();
            this.modalClose.emit();
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();

    }
  }
}
