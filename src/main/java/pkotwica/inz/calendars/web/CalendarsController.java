package pkotwica.inz.calendars.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.event.web.form.CalendarInfoData;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@Controller
@RequestMapping
public class CalendarsController {

    private final UserService userService;
    private final EventService eventService;
    private final ProjectService projectService;
    private final RoomService roomService;

    public CalendarsController(UserService userService, EventService eventService, ProjectService projectService, RoomService roomService) {
        this.userService = userService;
        this.eventService = eventService;
        this.projectService = projectService;
        this.roomService = roomService;
    }

    @GetMapping
    public String account() {
        return "account-page";
    }

    @GetMapping({"my-calendar/{month}/{year}", "my-calendar/{month}", "my-calendar"})
    public String myCalendar(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, Model model) {
        model.addAttribute("calendarInfo", eventService.getCalendarInfo(month, year));
        model.addAttribute("eventTypes", eventService.getEventTypes());
        model.addAttribute("rooms", roomService.getAllRoomsData());
        return "my-calendar";
    }

    @GetMapping({
            PROTECTED_API_PREFIX + "/my-calendar/{month}/{year}",
            PROTECTED_API_PREFIX + "/my-calendar/{month}",
            PROTECTED_API_PREFIX + "/my-calendar"
    })
    @ResponseBody
    public CalendarInfoData myCalendarRest(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year) {
        return CalendarInfoData.of(eventService.getCalendarInfo(month, year));
    }

    @GetMapping({"users-calendars/{month}/{year}", "users-calendars/{month}", "users-calendars"})
    public String usersCalendars(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String email, Model model) {
        if (email != null) {
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByUser(month, year, email));
            model.addAttribute("eventTypes", eventService.getEventTypes());
        }
        model.addAttribute("users", userService.getUsersWithEmails());
        model.addAttribute("selectedEmail", email);

        return "users-calendars";
    }

    @GetMapping({
            PROTECTED_API_PREFIX + "/users-calendar/{month}/{year}",
            PROTECTED_API_PREFIX + "/users-calendar/{month}",
            PROTECTED_API_PREFIX + "/users-calendar"
    })
    @ResponseBody
    public CalendarInfoData usersCalendarsRest(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam String email) {
        return CalendarInfoData.of(eventService.getCalendarInfoByUser(month, year, email));
    }

    @GetMapping({"projects/{month}/{year}", "projects/{month}", "projects"})
    public String projects(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String project, Model model) {
        if (project != null) {
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByProject(month, year, project));
            model.addAttribute("users", projectService.getUsersDataByProject(project));
        }
        model.addAttribute("projects", projectService.getProjectsByLoggedUser());
        model.addAttribute("projectName", project);

        return "projects";
    }

    @GetMapping({
            PROTECTED_API_PREFIX + "/projects-calendar/{month}/{year}",
            PROTECTED_API_PREFIX + "/projects-calendar/{month}",
            PROTECTED_API_PREFIX + "/projects-calendar"
    })
    @ResponseBody
    public CalendarInfoData projectsApi(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam String project) {
        return CalendarInfoData.of(eventService.getCalendarInfoByProject(month, year, project));
    }

    @GetMapping({"rooms/{month}/{year}", "rooms/{month}", "rooms"})
    public String rooms(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam(required = false) String room, Model model) {
        if (room != null) {
            model.addAttribute("selectedRoom", room);
            model.addAttribute("calendarInfo", eventService.getCalendarInfoByRoom(month, year, room));
            model.addAttribute("loggedUserId", userService.getLoggedUser().getId());
            model.addAttribute("eventTypes", eventService.getEventTypes());
        }
        model.addAttribute("rooms", roomService.getAllRoomsData());

        return "rooms";
    }

    @GetMapping({
            PROTECTED_API_PREFIX + "/rooms-calendar/{month}/{year}",
            PROTECTED_API_PREFIX + "/rooms-calendar/{month}",
            PROTECTED_API_PREFIX + "/rooms-calendar"
    })
    @ResponseBody
    public CalendarInfoData roomsApi(@PathVariable(required = false) Integer month, @PathVariable(required = false) Integer year, @RequestParam String room) {
        return CalendarInfoData.of(eventService.getCalendarInfoByRoom(month, year, room));
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}