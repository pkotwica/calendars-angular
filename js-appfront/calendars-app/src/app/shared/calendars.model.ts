export interface StandardCalendarsApiResponse {
  success: boolean,
  messageInfo: string,
}
