package pkotwica.inz.calendars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pkotwica.inz.calendars.initialdata.InitialData;

@SpringBootApplication
public class CalendarsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CalendarsApplication.class, args);

        context.getBean(InitialData.class).load();
    }

}
