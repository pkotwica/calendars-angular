package pkotwica.inz.calendars.project.web;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.project.web.factory.EditProjectResponseFactory;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("admin-projects")
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class AdminProjectController {

    private final UserService userService;
    private final ProjectService projectService;

    public AdminProjectController(UserService userService, ProjectService projectService) {
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping
    public String getAdminAllProjects(Model model) {
        model.addAttribute("projects", projectService.getAllProjectsData());
        return "admin/admin-projects";
    }

    @GetMapping("project/{projectName}")
    public String getAdminProject(@PathVariable String projectName, Model model) {
        model.addAttribute("users", projectService.getUsersDataByProject(projectName));
        model.addAttribute("allUsers", projectService.getUsersWithEmailsNotInProject(projectName));
        model.addAttribute("project", projectName);
        return "admin/admin-project";
    }

    @PostMapping("add")
    @ResponseBody
    public CalendarsApiResponse addNewProject(@Valid @RequestBody EditProjectForm editProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.addByIsSuccess(projectService.addNewProject(editProjectForm));
    }

    @PostMapping("remove")
    @ResponseBody
    public CalendarsApiResponse removeProject(@Valid @RequestBody EditProjectForm editProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.removeByIsSuccess(projectService.removeProject(editProjectForm));
    }

    @PostMapping("add/user")
    @ResponseBody
    public CalendarsApiResponse addUserToProject(@Valid @RequestBody EditUserInProjectForm editUserInProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.addUserByIsSuccess(projectService.addUserToProject(editUserInProjectForm));
    }

    @PostMapping("remove/user")
    @ResponseBody
    public CalendarsApiResponse removeUserFromProject(@Valid @RequestBody EditUserInProjectForm editUserInProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.removeUserByIsSuccess(projectService.removeUserFromProject(editUserInProjectForm));
    }

    @ModelAttribute(name = "userRoles")
    public Collection<String> getAllUserRoles() {
        return userService.getAllUserRoles();
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
