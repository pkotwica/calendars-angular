module.exports = {
    mode: 'development',
    entry: ['./src/main/resources/static/js/calendars.js', "./src/main/resources/static/css/style.scss"],
    output: {
        filename: '../target/classes/static/js/script.js'
    },
    module: {
        rules: [
            {
                test: /\.(s*)css$/,
                use: [
                    "style-loader",
                    {
                        loader: "file-loader",
                        options: {
                            name: "../target/classes/static/css/[name].min.css"
                        }
                    },
                    "sass-loader"
                ]
            }
        ]
    },
};