import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  GetUserData,
  GetUserDataFailure,
  GetUserDataSuccess,
  GetUsersEmailsDataFailure,
  GetUsersEmailsDataSuccess,
  UserActionsTypes
} from '../actions/user.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {AuthService} from '../../auth/auth.service';
import {of} from 'rxjs';
import {UserService} from '../services/user.service';

@Injectable()
export class UserEffects {

  @Effect()
  getUserData = this.actions$.pipe(
    ofType(UserActionsTypes.GetUserData),
    exhaustMap(() => this.userService.getUserData().pipe(
      map(user => new GetUserDataSuccess(user)),
      catchError(error => {
        return of(new GetUserDataFailure())
      }),
      )
    ),
  );

  @Effect()
  changeUserLogin = this.actions$.pipe(
    ofType(UserActionsTypes.ChangeUserLoginSuccess),
    map(() => new GetUserData()),
  );

  @Effect()
  getUsersEmailsData = this.actions$.pipe(
    ofType(UserActionsTypes.GetUsersEmailsData),
    exhaustMap(() => this.userService.getUsersEmails().pipe(
      map(users => new GetUsersEmailsDataSuccess(users)),
      catchError(error => {
        return of(new GetUsersEmailsDataFailure())
      }),
      )
    ),
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private userService: UserService,
  ) {
  }
}
