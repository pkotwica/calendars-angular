import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin.component';
import {AdminGuard} from './admin.guard';
import {AdminUsersComponent} from './users/admin-users.component';
import {AdminRoomsComponent} from './rooms/admin-rooms.component';
import {AdminProjectsComponent} from './projects/admin-projects.component';
import {AdminProjectComponent} from './projects/project/admin-project.component';

const adminRoutes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AdminGuard], children: [
      {path: 'users', component: AdminUsersComponent},
      {
        path: 'projects', children: [
          {path: '', component: AdminProjectsComponent},
          {path: ':projectName', component: AdminProjectComponent},
        ]
      },
      {path: 'rooms', component: AdminRoomsComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
