import {Data} from './common.interfaces';

export interface AuthState extends Data {
  id?: number;
  tokenCode: string;
  expirationDate: string;
}

export interface LoginForm {
  username: string;
  password: string;
}
