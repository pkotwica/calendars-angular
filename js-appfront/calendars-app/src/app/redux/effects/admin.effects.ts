import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  AdminActionsTypes,
  GetAdminRooms,
  GetAdminRoomsFailure,
  GetAdminRoomsSuccess,
  GetAdminsProjects,
  GetAdminsProjectsFailure,
  GetAdminsProjectsInfo,
  GetAdminsProjectsInfoFailure,
  GetAdminsProjectsInfoSuccess,
  GetAdminsProjectsSuccess,
  GetUsers,
  GetUsersFailure,
  GetUsersSuccess
} from '../actions/admin.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {AdminService} from '../services/admin.service';
import {of} from 'rxjs';
import {Injectable} from '@angular/core';
import {GetRooms} from '../actions/room.actions';

@Injectable()
export class AdminEffects {

  @Effect()
  $getUsersEffect = this.actions$.pipe(
    ofType(AdminActionsTypes.GetUsers),
    exhaustMap((action: GetUsers) => this.adminService.getUserToAdministrate(action.payload).pipe(
      map(pageableUsers => new GetUsersSuccess(pageableUsers)),
      catchError(error => of(new GetUsersFailure())),
    ))
  );

  @Effect()
  $getRoomsEffect = this.actions$.pipe(
    ofType(AdminActionsTypes.GetRooms),
    exhaustMap((action: GetAdminRooms) => this.adminService.getRoomsToAdministrate(action.payload).pipe(
      map(pageableRooms => new GetAdminRoomsSuccess(pageableRooms)),
      catchError(error => of(new GetAdminRoomsFailure())),
    ))
  );

  @Effect()
  $getRoomsSuccessEffect = this.actions$.pipe(
    ofType(AdminActionsTypes.GetRoomsSuccess),
    map(() => new GetRooms()),
  );

  @Effect()
  $getProjectsEffect = this.actions$.pipe(
    ofType(AdminActionsTypes.GetAdminsProjects),
    exhaustMap((action: GetAdminsProjects) => this.adminService.getProjectsToAdministrate(action.payload).pipe(
      map(pageableProjects => new GetAdminsProjectsSuccess(pageableProjects)),
      catchError(error => of(new GetAdminsProjectsFailure())),
    ))
  );

  @Effect()
  $getProjectsInfoEffect = this.actions$.pipe(
    ofType(AdminActionsTypes.GetAdminsProjectsInfo),
    exhaustMap((action: GetAdminsProjectsInfo) => this.adminService.getProjectsInfo(action.payload).pipe(
      map(projectsInfoData => new GetAdminsProjectsInfoSuccess(projectsInfoData)),
      catchError(error => of(new GetAdminsProjectsInfoFailure())),
    ))
  );

  constructor(
    private actions$: Actions,
    private adminService: AdminService,
  ) {
  }
}
