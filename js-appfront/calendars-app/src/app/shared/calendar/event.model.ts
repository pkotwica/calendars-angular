export interface SaveEventModel {
  id: string;
  form: any;
  year: number;
  month: number;
  day: number;
}
