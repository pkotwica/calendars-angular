package pkotwica.inz.calendars.project.web;

import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.administration.project.AdminProjectFacade;
import pkotwica.inz.calendars.project.web.factory.EditProjectResponseFactory;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.project.web.form.ProjectData;
import pkotwica.inz.calendars.project.web.form.ProjectInfo;
import pkotwica.inz.calendars.web.common.AbstractPageableController;
import pkotwica.inz.calendars.web.common.PageableResponse;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@RestController
@RequestMapping(PROTECTED_API_PREFIX + "/admin/projects")
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class AdministrationProjectRestController extends AbstractPageableController {

    private AdminProjectFacade adminProjectFacade;

    public AdministrationProjectRestController(AdminProjectFacade adminProjectFacade) {
        this.adminProjectFacade = adminProjectFacade;
    }

    @GetMapping
    public PageableResponse<List<ProjectData>> getAdminAllProjects(@RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
                                                                   @RequestParam(value = "size", required = false, defaultValue = "10") int pageSize,
                                                                   @RequestParam(value = "search", required = false) String searchPhrase) {
        return pageableResponseOf(adminProjectFacade.findRoomsToAdministrate(--pageNumber, pageSize, searchPhrase));
    }

    @GetMapping("/project/{projectName}")
    public ProjectInfo getAdminProject(@PathVariable String projectName) {
        return adminProjectFacade.getProjectUsers(projectName);
    }

    @PostMapping("add")
    public CalendarsApiResponse addNewProject(@Valid @RequestBody EditProjectForm editProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.addByIsSuccess(adminProjectFacade.addProject(editProjectForm));
    }

    @PostMapping("edit")
    public CalendarsApiResponse editProject(@Valid @RequestBody EditProjectForm editProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors() || editProjectForm.getOldProjectName() == null) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.changeByIsSuccess(adminProjectFacade.editProject(editProjectForm));
    }

    @PostMapping("remove")
    public CalendarsApiResponse removeProject(@Valid @RequestBody EditProjectForm editProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.removeByIsSuccess(adminProjectFacade.removeProject(editProjectForm));
    }

    @PostMapping("add/user")
    public CalendarsApiResponse addUserToProject(@Valid @RequestBody EditUserInProjectForm editUserInProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.addUserByIsSuccess(adminProjectFacade.addUserToProject(editUserInProjectForm));
    }

    @PostMapping("remove/user")
    public CalendarsApiResponse removeUserFromProject(@Valid @RequestBody EditUserInProjectForm editUserInProjectForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            EditProjectResponseFactory.fillAllError();
        }
        return EditProjectResponseFactory.removeUserByIsSuccess(adminProjectFacade.removeUserFromProject(editUserInProjectForm));
    }
}
