import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DayData, EventData} from '../../../redux/interfaces/calendar.interfaces';

@Component({
  selector: 'app-day',
  templateUrl: 'day.component.html',
  styleUrls: ['day.component.scss'],
})
export class DayComponent {
  @Input()
  dayData: DayData;
  @Input()
  editable: boolean = true;
  @Input()
  currentUserEmail: string;

  @Output()
  eventClickedEmitter = new EventEmitter<EventData>();
  @Output()
  dayClickedEmitter = new EventEmitter<DayData>();

  eventClicked(event: EventData) {
    if (!this.dayData.empty && this.editable && event.ownerEmail === this.currentUserEmail) {
      this.eventClickedEmitter.emit(event);
    }
  }

  dayClicked() {
    if (!this.dayData.empty && this.editable) {
      this.dayClickedEmitter.emit(this.dayData);
    }
  }
}
