package pkotwica.inz.calendars.event.web.form;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Getter
@Setter
@NoArgsConstructor
@Builder
public class CalendarInfoData {
    private Month currentMonth;
    private Month prevMonth;
    private Month nextMonth;
    private Integer prevYear;
    private Integer nextYear;
    private Integer currentYear;
    private Integer startEmptyDays;
    private Integer endEmptyDays;
    private List<DayData> days;

    private CalendarInfoData(Month currentMonth, Month prevMonth, Month nextMonth, Integer prevYear, Integer nextYear, Integer currentYear, Integer startEmptyDays, Integer endEmptyDays, List<DayData> days) {
        this.currentMonth = currentMonth;
        this.prevMonth = prevMonth;
        this.nextMonth = nextMonth;
        this.prevYear = prevYear;
        this.nextYear = nextYear;
        this.currentYear = currentYear;
        this.startEmptyDays = startEmptyDays;
        this.endEmptyDays = endEmptyDays;
        this.days = days;
    }

    public static CalendarInfoData of(CalendarInfo calendarInfo) {
        List<DayData> dayDataDataList = new ArrayList<>();
        addEmptyDays(dayDataDataList, calendarInfo.getStartEmptyDays());
        calendarInfo.getDays().forEach(day -> dayDataDataList.add(DayData.of(day)));
        addEmptyDays(dayDataDataList, calendarInfo.getEndEmptyDays());

        return new CalendarInfoData(
                calendarInfo.getCurrentMonth(),
                calendarInfo.getPrevMonth(),
                calendarInfo.getNextMonth(),
                calendarInfo.getPrevYear(),
                calendarInfo.getNextYear(),
                calendarInfo.getCurrentYear(),
                calendarInfo.getStartEmptyDays(),
                calendarInfo.getEndEmptyDays(),
                dayDataDataList
        );
    }

    private static void addEmptyDays(List<DayData> dayDataDataList, Integer startEmptyDays) {
        IntStream.rangeClosed(1, startEmptyDays).forEach(num -> dayDataDataList.add(DayData.empty()));
    }
}
