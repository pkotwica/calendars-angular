package pkotwica.inz.calendars.room.web;

import org.apache.logging.log4j.util.Strings;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.administration.room.AdminRoomFacade;
import pkotwica.inz.calendars.room.web.factory.EditRoomResponseFactory;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.room.web.form.RoomData;
import pkotwica.inz.calendars.web.common.AbstractPageableController;
import pkotwica.inz.calendars.web.common.PageableResponse;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@RestController
@RequestMapping(PROTECTED_API_PREFIX + "/admin/rooms")
@Secured({"ROLE_ADMIN", "ROLE_MODERATOR"})
public class AdministrationRoomRestController extends AbstractPageableController {

    AdminRoomFacade adminRoomFacade;

    public AdministrationRoomRestController(AdminRoomFacade adminRoomFacade) {
        this.adminRoomFacade = adminRoomFacade;
    }

    @GetMapping
    public PageableResponse<List<RoomData>> getPageableRooms(
            @RequestParam(value = "page", required = false, defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", required = false, defaultValue = "10") int pageSize,
            @RequestParam(value = "search", required = false) String searchPhrase) {

        return pageableResponseOf(adminRoomFacade.findRoomsToAdministrate(--pageNumber, pageSize, searchPhrase));
    }

    @PostMapping("add")
    public CalendarsApiResponse addNewRoom(@Valid @RequestBody EditRoomForm editRoomForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.addByIsSuccess(adminRoomFacade.addNewRoom(editRoomForm));
    }

    @PostMapping("edit")
    public CalendarsApiResponse editRoom(@Valid @RequestBody EditRoomForm editRoomForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.editByIsSuccess(adminRoomFacade.editRoom(editRoomForm));
    }

    @PostMapping("remove")
    public CalendarsApiResponse removeRoom(@RequestBody String roomName) {
        if (Strings.isEmpty(roomName)) {
            return EditRoomResponseFactory.fillAllError();
        }
        return EditRoomResponseFactory.removeByIsSuccess(adminRoomFacade.removeRoom(roomName));
    }
}
