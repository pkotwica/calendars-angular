package pkotwica.inz.calendars.event.web.form;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class DayData {
    private boolean isEmpty;
    private Integer number;
    private Integer numberOfWeekDay;
    private String name;
    private List<EventData> events;

    private DayData(Integer number, Integer numberOfWeekDay, String name, List<EventData> events) {
        this.isEmpty = false;
        this.number = number;
        this.numberOfWeekDay = numberOfWeekDay;
        this.name = name;
        this.events = events;
    }

    private DayData(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public static DayData of(Day day) {
        List<EventData> eventDataList = new ArrayList<>();
        day.getEvents().forEach(event -> eventDataList.add(EventData.of(event)));

        return new DayData(
                day.getNumber(),
                day.getNumberOfWeekDay(),
                day.getName(),
                eventDataList
        );
    }

    public static DayData empty() {
        return new DayData(true);
    }
}
