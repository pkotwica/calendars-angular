import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {
  CalendarData,
  DayData,
  EventData,
  GetCalendarDetails,
  RoomsDataState
} from '../../../redux/interfaces/calendar.interfaces';
import {Observable, of, Subscription} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../../app.redux';
import {selectEventsTypesState, selectRoomsState} from '../../../redux/selectors/calendar.selector';
import {NgForm} from '@angular/forms';
import {catchError} from 'rxjs/operators';
import {CalendarService} from '../../../redux/services/calendar.service';
import {SaveEventModel} from '../event.model';

@Component({
  selector: 'app-manage-event-modal',
  templateUrl: 'manage-event-modal.component.html',
})
export class ManageEventModalComponent implements OnInit, OnDestroy {

  modalTitle: string = '';
  editEventModalTitle = 'Edit Event -';
  newEventModalTitle = 'Add New Event -';
  dateRegex = /[0-2][0-9]:[0-5][0-9]/g;
  eventsTypes$: Observable<String[]> = this.state.select(selectEventsTypesState);
  rooms$: Observable<RoomsDataState> = this.state.select(selectRoomsState);

  @Input()
  isEdit: boolean = false;
  @Input()
  event: EventData;
  @Input()
  day: DayData;
  @Input()
  calendarData: CalendarData;
  @Input()
  roomSelected: string;

  @ViewChild('manageEventForm', {static: true})
  manageEventForm: NgForm;
  changeFormSubs: Subscription;
  errorMessage: string = '';
  datesProvided: boolean = false;

  @Output()
  modalClose = new EventEmitter();
  @Output()
  refreshCalendarEvent = new EventEmitter<GetCalendarDetails>();

  constructor(
    private state: Store<State>,
    private calendarService: CalendarService,
  ) {
  }

  ngOnInit(): void {
    this.setModalTitle();
    this.changeFormSubs = this.manageEventForm.form.valueChanges.subscribe(() => {
      this.checkTimes();
    })
  }

  private setModalTitle() {
    if (this.isEdit) {
      const date = new Date(this.event.startDate);
      this.modalTitle = `${this.editEventModalTitle} ${date.getDate()}.${date.getMonth()}.${date.getFullYear()}`
    } else {
      this.modalTitle = `${this.newEventModalTitle} ${this.day.number}.${this.calendarData.currentMonth.number}.${this.calendarData.currentYear}`;
    }
  }

  submitForm(event: EventData) {
    if (this.manageEventForm.valid && this.datesProvided) {
      this.calendarService.saveEvent(this.prepareSaveEventModel()).pipe(
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe(submitInfo => {
        if (submitInfo.success) {
          this.handleSuccessResponse();
        } else {
          this.errorMessage = submitInfo.messageInfo;
        }
      });
    }
  }

  handleSuccessResponse() {
    this.refreshCalendarEvent.emit({
      monthNumber: this.calendarData.currentMonth.number,
      year: this.calendarData.currentYear
    });
    this.errorMessage = null;
    this.manageEventForm.reset();
    this.modalClose.emit();
  }

  onDeleteEvent() {
    if (this.isEdit) {
      this.calendarService.deleteEvent(this.event.id).pipe(
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe(submitInfo => {
        if (submitInfo.success) {
          this.handleSuccessResponse();
        } else {
          this.errorMessage = submitInfo.messageInfo;
        }
      });
    }
  }

  checkTimes() {
    const startTimeInput = this.manageEventForm.value.startTime;
    const endTimeInput = this.manageEventForm.value.endTime;
    this.datesProvided = (
      this.dateMatches(startTimeInput) &&
      this.dateMatches(endTimeInput) &&
      this.startTimeIsBeforeEndTime(startTimeInput, endTimeInput)
    );
  }

  private dateMatches(inputTime) {
    if (inputTime && inputTime.match(this.dateRegex)) {
      const dateUnits = inputTime.split(':');
      return +dateUnits[0] >= 0 && +dateUnits[0] <= 23 && +dateUnits[1] >= 0 && +dateUnits[1] <= 59;
    }
    return false;
  }

  private startTimeIsBeforeEndTime(startInputTime: string, endInputTime: string) {
    const startTimeUnits = startInputTime.split(':');
    const endTimeUnits = endInputTime.split(':');
    return +startTimeUnits[0] < +endTimeUnits[0] || (+startTimeUnits[0] == +endTimeUnits[0] && +startTimeUnits[1] < +endTimeUnits[1]);
  }

  ngOnDestroy(): void {
    this.changeFormSubs.unsubscribe();
  }

  private prepareSaveEventModel(): SaveEventModel {
    return {
      id: this.isEdit ? this.event.id.toString() : '',
      form: this.manageEventForm.value,
      year: this.calendarData.currentYear,
      month: this.calendarData.currentMonth.number,
      day: this.day.number,
    };
  }

  getRoomToSelector(): string {
    if (this.roomSelected) {
      return this.roomSelected;
    }
    this.isEdit ? this.event.room : ''
  }
}
