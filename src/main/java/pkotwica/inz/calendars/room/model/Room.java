package pkotwica.inz.calendars.room.model;

import lombok.*;
import pkotwica.inz.calendars.event.model.Event;

import javax.persistence.*;
import java.util.List;

@Entity(name = "rooms")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String name;
    @Column(unique = true)
    private String number;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "room", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Event> events;
}
