package pkotwica.inz.calendars.web.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageableResponse<T> {
    private Integer currentPage;
    private Integer totalPages;
    private T data;

    public PageableResponse(Integer currentPage, Integer totalPages, T data) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.data = data;
    }


}
