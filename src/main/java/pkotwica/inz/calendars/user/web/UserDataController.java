package pkotwica.inz.calendars.user.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;
import pkotwica.inz.calendars.user.web.factory.ChangeUserDataResponseFactory;
import pkotwica.inz.calendars.user.web.form.ChangeLoginForm;
import pkotwica.inz.calendars.user.web.form.ChangePassForm;
import pkotwica.inz.calendars.user.web.form.UserWithEmail;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@RestController
@RequestMapping(PROTECTED_API_PREFIX)
public class UserDataController {
    @AllArgsConstructor
    @Getter
    @Setter
    static
    class UsersEmails {
        private List<UserWithEmail> data;
    }

    @AllArgsConstructor
    @Getter
    @Setter
    static
    class UsersRoles {
        private Collection<String> roles;
    }

    private final static String CHANGE_USER_DATA_PREFIX = "change";

    private UserService userService;

    public UserDataController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("user")
    public UserData getActiveUserData() {
        return userService.getUserData();
    }

    @GetMapping("users")
    public UsersEmails getUsersEmails() {
        return new UsersEmails(userService.getUsersWithEmails());
    }

    @GetMapping("users/roles")
    public UsersRoles getUsersRoles() {
        return new UsersRoles(userService.getAllUserRoles());
    }

    @PostMapping("user/" + CHANGE_USER_DATA_PREFIX + "/login")
    public CalendarsApiResponse changeLogin(@Valid @RequestBody ChangeLoginForm changeLoginForm, BindingResult bindingResult) {
        return ChangeUserDataResponseFactory.changeLoginResponse(!bindingResult.hasErrors() && userService.changeLogin(changeLoginForm));
    }

    @PostMapping("user/" + CHANGE_USER_DATA_PREFIX + "/pass")
    public CalendarsApiResponse changePass(@Valid @RequestBody ChangePassForm changePassForm, BindingResult bindingResult) {
        return ChangeUserDataResponseFactory.changePassResponse(!bindingResult.hasErrors() && userService.changePass(changePassForm));
    }
}
