import {AdminActions, AdminActionsTypes} from '../actions/admin.actions';
import {AdminState} from '../interfaces/admin.interface';

const defaultAdminState: AdminState = undefined;

export const adminsStateReducer = (state: AdminState = defaultAdminState, action: AdminActions) => {
  switch (action.type) {
    case AdminActionsTypes.GetUsers: {
      return {
        ...state,
        usersToAdministrate: {
          status: {
            error: false,
            loading: true,
            errorMessage: '',
          }
        }
      };
    }
    case AdminActionsTypes.GetUsersSuccess: {
      return {
        ...state,
        usersToAdministrate: {
          data: action.payload.data,
          currentPage: action.payload.currentPage,
          totalPages: action.payload.totalPages,
          status: {
            error: false,
            loading: false,
          }
        }
      };
    }
    case AdminActionsTypes.GetUsersFailure: {
      return {
        ...state,
        usersToAdministrate: {
          status: {
            error: true,
            loading: false,
            errorMessage: 'Error occurred, please try again later.',
          }
        }
      };
    }
    case AdminActionsTypes.GetRooms: {
      return {
        ...state,
        roomsToAdministrate: {
          status: {
            error: false,
            loading: true,
            errorMessage: '',
          }
        }
      };
    }
    case AdminActionsTypes.GetRoomsSuccess: {
      return {
        ...state,
        roomsToAdministrate: {
          data: action.payload.data,
          currentPage: action.payload.currentPage,
          totalPages: action.payload.totalPages,
          status: {
            error: false,
            loading: false,
          }
        }
      };
    }
    case AdminActionsTypes.GetRoomsFailure: {
      return {
        ...state,
        usersToAdministrate: {
          status: {
            error: true,
            loading: false,
            errorMessage: 'Error occurred, please try again later.',
          }
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjects: {
      return {
        ...state,
        projectAdministration: {
          projectsToAdministrate: {
            status: {
              error: false,
              loading: true,
              errorMessage: '',
            }
          },
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjectsSuccess: {
      return {
        ...state,
        projectAdministration: {
          projectsToAdministrate: {
            data: action.payload.data,
            currentPage: action.payload.currentPage,
            totalPages: action.payload.totalPages,
            status: {
              error: false,
              loading: false,
            }
          },
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjectsFailure: {
      return {
        ...state,
        projectAdministration: {
          projectsToAdministrate: {
            status: {
              error: true,
              loading: false,
              errorMessage: 'Error occurred, please try again later.',
            }
          },
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjectsInfo: {
      return {
        ...state,
        projectAdministration: {
          projectInfoData: {
            status: {
              error: false,
              loading: true,
              errorMessage: '',
            }
          },
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjectsInfoSuccess: {
      return {
        ...state,
        projectAdministration: {
          projectInfoData: {
            name: action.payload.name,
            members: action.payload.members,
            notMembers: action.payload.notMembers,
            status: {
              error: false,
              loading: false,
            }
          },
        }
      };
    }
    case AdminActionsTypes.GetAdminsProjectsInfoFailure: {
      return {
        ...state,
        projectAdministration: {
          projectsToAdministrate: {
            status: {
              error: true,
              loading: false,
              errorMessage: 'Error occurred, please try again later.',
            }
          },
        }
      };
    }
    default: {
      return state;
    }
  }
};
