import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-pageable-selector',
  templateUrl: 'pageable-selector.component.html',
  styleUrls: ['pageable-selector.component.scss'],
})
export class PageableSelectorComponent implements OnInit {

  @Input()
  currentPage: number;
  @Input()
  totalPages: number;

  @Output()
  pageSelected = new EventEmitter<number>();

  windowBreakpoint: number = 992;
  windowWidth: number;
  maxSeePagesSmall: number = 3;
  maxSeePagesLarge: number = 7;

  ngOnInit(): void {
    // this.windowWidth = window.innerWidth;
  }

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   this.windowWidth = window.innerWidth;
  // }

  rangeOfPages(): Array<number> {
    // const maxSeePages = this.windowWidth >= this.windowBreakpoint ? this.maxSeePagesLarge : this.maxSeePagesSmall;
    // const startPage = this.currentPage === 1 ? 1 : this.currentPage - ((maxSeePages - 1) / 2);
    // const endPage = this.currentPage === 1 ? maxSeePages : this.currentPage + ((maxSeePages - 1) / 2);
    //
    // return this.getRangeOfNumbers(startPage, endPage > this.totalPages ? this.totalPages : endPage);

    return this.getRangeOfNumbers(1, this.totalPages);
  }

  thisPage(pageNumber: number) {
    if (this.currentPage != pageNumber) {
      this.pageSelected.emit(pageNumber);
    }
  }

  prevPage() {
    this.pageSelected.emit(this.currentPage - 1);
  }

  nextPage() {
    this.pageSelected.emit(this.currentPage + 1);
  }

  // isLeftExpander(): boolean {
  //   const maxSeePages = this.windowWidth >= this.windowBreakpoint ? this.maxSeePagesLarge : this.maxSeePagesSmall;
  //
  //   return
  // }
  //
  // isRightExpander(): boolean {
  //   const maxSeePages = this.windowWidth >= this.windowBreakpoint ? this.maxSeePagesLarge : this.maxSeePagesSmall;
  //
  // }

  private getRangeOfNumbers(start: number, end: number): Array<number> {
    return Array.from({length: (end + 1) - start}, (value, key) => key + start);
  }
}
