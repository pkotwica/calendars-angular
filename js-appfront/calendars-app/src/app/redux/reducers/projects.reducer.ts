import {OwnedProjectsDataState} from '../interfaces/calendar.interfaces';
import {initialLoadingStatus} from '../interfaces/common.interfaces';
import {ProjectsActions, ProjectsActionsTypes} from '../actions/projects.actions';

const initialOwnedProjectsDataState: OwnedProjectsDataState = {
  status: initialLoadingStatus,
};

export const ownedProjectsDataReducer = (state: OwnedProjectsDataState = initialOwnedProjectsDataState, action: ProjectsActions) => {
  switch (action.type) {
    case ProjectsActionsTypes.GetOwnedProjects: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case ProjectsActionsTypes.GetOwnedProjectsSuccess: {
      return {
        ...state,
        projects: action.payload.projects,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case ProjectsActionsTypes.GetOwnedProjectsFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    case ProjectsActionsTypes.GetProjectUsers: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case ProjectsActionsTypes.GetProjectUsersSuccess: {
      return {
        ...state,
        users: action.payload.users,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case ProjectsActionsTypes.GetProjectUsersFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }
};
