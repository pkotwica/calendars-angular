import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {selectProjectsInfo} from '../../../../redux/selectors/admin.selector';
import {ActivatedRoute, Params} from '@angular/router';
import {GetAdminsProjectsInfo} from '../../../../redux/actions/admin.actions';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {AdminService} from '../../../../redux/services/admin.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-admin-project',
  templateUrl: 'admin-project.component.html',
  styleUrls: ['admin-project.component.scss'],
})
export class AdminProjectComponent implements OnInit {

  projectName: string;

  projectInfo$ = this.state.select(selectProjectsInfo);

  isEditProjectModal: boolean = false;
  errorMessage: string = null;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private adminService: AdminService,
  ) {
  }

  ngOnInit(): void {
    this.projectName = this.route.snapshot.params['projectName'];
    this.state.dispatch(new GetAdminsProjectsInfo(this.projectName));

    this.route.params.subscribe((params: Params) => {
      this.projectName = params['projectName'];
      this.state.dispatch(new GetAdminsProjectsInfo(this.projectName));
    })
  }

  removeUser(email: string) {
    this.adminService.removeUserFromProject(email, this.projectName).pipe(
      tap(submitInfo => {
        if (submitInfo.success) {
          this.errorMessage = null;
          this.state.dispatch(new GetAdminsProjectsInfo(this.projectName));
        } else {
          this.errorMessage = submitInfo.messageInfo;
        }
      }),
      catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
    ).subscribe();
  }

  addUser(addUserFormValue: NgForm) {
    if (addUserFormValue.valid) {
      this.adminService.addUserToProject(addUserFormValue.value.userEmail, this.projectName).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            this.errorMessage = null;
            this.state.dispatch(new GetAdminsProjectsInfo(this.projectName));
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }
}
