import {Action} from '@ngrx/store';
import {UserDataState, UsersEmailsDataState} from '../interfaces/user.interfaces';

export enum UserActionsTypes {
  GetUserData = '[User] get user details',
  GetUserDataSuccess = '[User] get user details success',
  GetUserDataFailure = '[User] get user details failure',
  ChangeUserLoginSuccess = '[User] change user login success',
  ResetUserData = '[User] reset user details',

  GetUsersEmailsData = '[Users] get emails',
  GetUsersEmailsDataSuccess = '[Users] get emails success',
  GetUsersEmailsDataFailure = '[Users] get emails failure',
}


export class GetUserData implements Action {
  readonly type = UserActionsTypes.GetUserData;
}

export class GetUserDataSuccess implements Action {
  readonly type = UserActionsTypes.GetUserDataSuccess;

  constructor(public payload: UserDataState) {
  }
}

export class GetUserDataFailure implements Action {
  readonly type = UserActionsTypes.GetUserDataFailure;
}

export class ChangeUserLoginSuccess implements Action {
  readonly type = UserActionsTypes.ChangeUserLoginSuccess;
}

export class ResetUserData implements Action {
  readonly type = UserActionsTypes.ResetUserData;
}

export class GetUsersEmailsData implements Action {
  readonly type = UserActionsTypes.GetUsersEmailsData;
}

export class GetUsersEmailsDataSuccess implements Action {
  readonly type = UserActionsTypes.GetUsersEmailsDataSuccess;

  constructor(public payload: UsersEmailsDataState) {
  }
}

export class GetUsersEmailsDataFailure implements Action {
  readonly type = UserActionsTypes.GetUsersEmailsDataFailure;
}

export type UserActions =
  GetUserData |
  GetUserDataSuccess |
  GetUserDataFailure |
  ChangeUserLoginSuccess |
  ResetUserData |
  GetUsersEmailsData |
  GetUsersEmailsDataSuccess |
  GetUsersEmailsDataFailure;
