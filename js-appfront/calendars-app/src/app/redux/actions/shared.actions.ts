import {Action} from '@ngrx/store';

export enum SharedActionsTypes {
  ResetStates = '[Shared] Reset all states',
}

export class ResetAllStates implements Action {
  readonly type = SharedActionsTypes.ResetStates;
}

export type SharedActions = ResetAllStates;
