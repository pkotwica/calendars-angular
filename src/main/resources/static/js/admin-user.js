import {setOption} from './my-calendar';
import {
    callPostToCalendarsApi,
    getSelectedOption,
} from './common/utils';

let removingEmail = undefined;
let changingPasswordEmail = undefined;
let userDataset = undefined;

export const bindAdminUser = () => {
    const adminUserPage = document.getElementById('js-admin-users');

    if (adminUserPage) {
        bindChangePasswordButtons();
        bindSavePasswordButton();
        bindEditUserButtons();
        bindSaveUsersButton();
        bindRemoveUserButtons();
        bindRemoveUserButton();
    }
};

const bindChangePasswordButtons = () => {
    const changePassButtons = document.getElementsByClassName('js-change-password');

    for (let button of changePassButtons) {
        button.addEventListener('click', function () {
            changingPasswordEmail = button.dataset.email;
            $('#changePasswordModal').modal('toggle');
        })
    }
};

const bindSavePasswordButton = () => {
    const savePassButton = document.getElementById('savePasswordModalButton');

    if (savePassButton) {
        savePassButton.addEventListener('click', function () {
            const body = {
                userEmail: changingPasswordEmail,
                newPassword: document.getElementById('changePasswordInput').value,
                repeatPassword: document.getElementById('changePasswordRepInput').value,
            };
            callPostToCalendarsApi('/admin-user/password', body, document.getElementById('passwordChangedMessage'));
        });
    }
};

const bindEditUserButtons = () => {
    const editUserButtons = document.getElementsByClassName('js-edit-user');

    for (let button of editUserButtons) {
        button.addEventListener('click', function () {
            userDataset = button.dataset;
            populateEditUserModal();
            $('#editUserDataModal').modal('toggle');
        })
    }
};

const populateEditUserModal = () => {
    document.getElementById('firstNameEditInput').value = userDataset.first;
    document.getElementById('lastNameEditInput').value = userDataset.last;
    document.getElementById('loginEditInput').value = userDataset.login;
    document.getElementById('jobPositionEditInput').value = userDataset.job;
    document.getElementById('emailEditInput').value = userDataset.email;
    setOption(document.getElementById('roleEditSelect'), userDataset.role);
};

const bindSaveUsersButton = () => {
    const saveUserDataButton = document.getElementById('saveUserDataModalButton');

    if (saveUserDataButton) {
        saveUserDataButton.addEventListener('click', function () {
            const body = {
                oldUserEmail: userDataset.email,
                firstName: document.getElementById('firstNameEditInput').value,
                lastName: document.getElementById('lastNameEditInput').value,
                login: document.getElementById('loginEditInput').value,
                jobPosition: document.getElementById('jobPositionEditInput').value,
                email: document.getElementById('emailEditInput').value,
                role: getSelectedOption(document.getElementById('roleEditSelect')),
            };
            callPostToCalendarsApi('/admin-user/edit', body, document.getElementById('editedUserDataMessage'));
        });
    }
};

const bindRemoveUserButtons = () => {
    const removeUserButtons = document.getElementsByClassName('js-remove-user');

    for (let button of removeUserButtons) {
        button.addEventListener('click', function () {
            removingEmail = button.dataset.email;
            $('#removeUserModal').modal('toggle');
        })
    }
};

const bindRemoveUserButton = () => {
    const removeUserButton = document.getElementById('removeUserModalButton');

    if (removeUserButton) {
        removeUserButton.addEventListener('click', function () {
            const body = {
                userEmail: removingEmail,
            };
            callPostToCalendarsApi('/admin-user/remove', body, document.getElementById('removeUserModalMessage'));
        });
    }
};