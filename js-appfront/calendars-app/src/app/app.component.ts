import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {Router, RouterEvent} from '@angular/router';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'calendars-app';

  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.autoLoginWithMapping();
  }

  private autoLoginWithMapping() {
    this.router.events.pipe(take(1)).subscribe((appEnterUrlEvent: RouterEvent) => {
      this.authService.autoLogin(appEnterUrlEvent.url);
    })
  }
}
