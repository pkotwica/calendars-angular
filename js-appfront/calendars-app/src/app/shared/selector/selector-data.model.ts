export interface SelectorDataModel {
    values: SelectorOptionModel[];
}

export interface SelectorOptionModel {
  value: string;
  name: string;
}
