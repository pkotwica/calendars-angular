import {initialLoadingStatus} from '../interfaces/common.interfaces';
import {RoomsActionsTypes} from '../actions/room.actions';
import {SystemRolesDataState} from '../interfaces/user.interfaces';
import {RolesActions, RolesActionsTypes} from '../actions/roles.actions';

const initialRolesDataState: SystemRolesDataState = {
  status: initialLoadingStatus,
};

export const systemRolesDataReducer = (state: SystemRolesDataState = initialRolesDataState, action: RolesActions) => {
  switch (action.type) {
    case RolesActionsTypes.GetRoles: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case RolesActionsTypes.GetRolesSuccess: {
      return {
        ...state,
        roles: action.payload.roles,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case RolesActionsTypes.GetRolesFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }
};
