package pkotwica.inz.calendars.event.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.event.model.Event;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.user.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Optional<Event> findEventById(long id);
    List<Event> findEventsByStartDateBetweenOrEndDateBetween(LocalDateTime startDateForStart, LocalDateTime endDateForStart, LocalDateTime startDateForEnd, LocalDateTime endDateForEnd);
    List<Event> findEventsByUserInAndStartDateBetweenOrderByStartDate(List<User> users, LocalDateTime startDate, LocalDateTime endDate);
    List<Event> findEventsByRoomAndStartDateBetweenOrderByStartDate(Room room, LocalDateTime startDate, LocalDateTime endDate);
}
