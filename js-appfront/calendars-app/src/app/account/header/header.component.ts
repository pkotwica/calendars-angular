import {Component} from '@angular/core';
import {State} from '../../app.redux';
import {Store} from '@ngrx/store';
import {AuthLogout} from '../../redux/actions/auth.actions';
import {AdminService} from '../../redux/services/admin.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss'],
})
export class HeaderComponent {

  constructor(
    private state: Store<State>,
    private adminService: AdminService,
  ) {
  }

  onLogout() {
    this.state.dispatch(new AuthLogout('You have been logout.'));
  }

  isUserModerator(): Observable<boolean> {
    return this.adminService.isUserModerator();
  }
}
