import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {select, Store} from '@ngrx/store';
import {State} from '../app.redux';
import {selectAuthState} from '../redux/selectors/auth.selector';
import {exhaustMap, take} from 'rxjs/operators';
import {AuthState} from '../redux/interfaces/authorization.interfaces';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  basicHash$: Observable<AuthState>;

  constructor(
    private authService: AuthService,
    private state: Store<State>
  ) {
    this.basicHash$ = state.pipe(select(selectAuthState));
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.basicHash$.pipe(
      take(1),
      exhaustMap((auth: AuthState) => {
        const token = this.authService.retrieveToken(auth);
        if (token) {
          let modifiedReq = req.clone({headers: req.headers.append('Authorization', `Bearer ${token}`)});
          return next.handle(modifiedReq);
        }
        return next.handle(req);
      }),
    );
  }

}
