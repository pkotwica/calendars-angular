package pkotwica.inz.calendars.authorization.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.authorization.model.AuthToken;

@Repository
public interface AuthTokenRepo extends JpaRepository<AuthToken, Long> {
}
