import {AuthState} from '../interfaces/authorization.interfaces';
import {AuthActions, AuthActionsTypes} from '../actions/auth.actions';
import {initialLoadingStatus} from '../interfaces/common.interfaces';

const initialAuthState: AuthState = {
  tokenCode: null,
  expirationDate: null,
  status: initialLoadingStatus,
};

export const authReducer = (state: AuthState = initialAuthState, action: AuthActions) => {
  switch(action.type) {
    case AuthActionsTypes.Auth: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case AuthActionsTypes.AuthSuccess: {
      return {
        ...state,
        tokenCode: action.payload.tokenCode,
        expirationDate: action.payload.expirationDate,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case AuthActionsTypes.AuthFailure: {
      return {
        ...state,
        status: {
          error: true,
          errorMessage: action.payload,
          loading: false,
        }
      }
    }
    case AuthActionsTypes.AuthLogout: {
      return {
        ...initialAuthState,
        status: {
          error: true,
          errorMessage: action.payload,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }

};
