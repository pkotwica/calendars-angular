import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {CalendarDataState, DayData, EventData, GetCalendarDetails} from '../../redux/interfaces/calendar.interfaces';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {selectCurrentUserEmail} from '../../redux/selectors/user.selector';

@Component({
  selector: 'app-calendar',
  templateUrl: 'calendar.component.html',
  styleUrls: ['calendar.component.scss'],
})
export class CalendarComponent {
  @Input()
  calendarDataState$: Observable<CalendarDataState>;
  @Input()
  editable: boolean = true;
  @Input()
  roomSelected: string;
  weekDuration: number = 7;

  currentUserEmail$ = this.state.select(selectCurrentUserEmail);
  manageEventModalIsOpen: boolean = false;
  isEditEvent: boolean = false;
  editingEvent: EventData;
  editingDay: DayData;

  @Output()
  changeMonth = new EventEmitter<GetCalendarDetails>();
  @Output()
  refresh = new EventEmitter();

  constructor(
    private state: Store<State>,
  ) {
  }

  range(startEmptyDays: number) {
    return Array.from({length: startEmptyDays}, (value, key) => key);
  }

  prevMonth(prevMonthNumber: number, currentYear: number) {
    const year = prevMonthNumber === 12 ? --currentYear : currentYear;
    this.changeMonth.emit({monthNumber: prevMonthNumber, year: year});
  }

  nextMonth(nextMonthNumber: number, currentYear: number) {
    const year = nextMonthNumber === 1 ? ++currentYear : currentYear;
    this.changeMonth.emit({monthNumber: nextMonthNumber, year: year});
  }

  onEventClicked(event: EventData, day: DayData) {
    this.isEditEvent = true;
    this.editingEvent = event;
    this.editingDay = day;
    this.manageEventModalIsOpen = true;
  }

  onDayClicked(day: DayData) {
    this.isEditEvent = false;
    this.editingDay = day;
    this.manageEventModalIsOpen = true;
  }
}
