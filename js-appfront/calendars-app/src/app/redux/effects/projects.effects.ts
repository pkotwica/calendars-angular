import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {OwnedProjectsDataState} from '../interfaces/calendar.interfaces';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {CalendarService} from '../services/calendar.service';
import {
  GetOwnedProjectsFailure,
  GetOwnedProjectsSuccess,
  GetProjectUsers,
  GetProjectUsersFailure,
  GetProjectUsersSuccess,
  ProjectsActionsTypes
} from '../actions/projects.actions';

@Injectable()
export class ProjectsEffects {

  @Effect()
  getOwnedProjects$ = this.actions$.pipe(
    ofType(ProjectsActionsTypes.GetOwnedProjects),
    exhaustMap(() => this.calendarService.getOwnedProjects().pipe(
      map((projects: OwnedProjectsDataState) => new GetOwnedProjectsSuccess(projects)),
      catchError(() => of(new GetOwnedProjectsFailure())),
    )),
  );

  @Effect()
  getProjectUsers$ = this.actions$.pipe(
    ofType(ProjectsActionsTypes.GetProjectUsers),
    exhaustMap((action: GetProjectUsers) => this.calendarService.getProjectUsers(action.payload).pipe(
      map((users: OwnedProjectsDataState) => new GetProjectUsersSuccess(users)),
      catchError(() => of(new GetProjectUsersFailure())),
    )),
  );

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private calendarService: CalendarService,
  ) {
  }
}
