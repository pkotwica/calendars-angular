import {Injectable} from '@angular/core';
import {PageableQueryInfo} from '../interfaces/pageable.interface';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class PageableService {
  private PAGE_PARAM_NAME = 'page';
  private SIZE_PARAM_NAME = 'size';
  private SEARCH_PARAM_NAME = 'search';


  createPageableRequestParams(params: PageableQueryInfo): HttpParams {
    let httParams = new HttpParams();
    httParams = params.page ? httParams.append(this.PAGE_PARAM_NAME, params.page.toString()) : httParams;
    httParams = params.size ? httParams.append(this.SIZE_PARAM_NAME, params.size.toString()) : httParams;
    httParams = params.searchPhrase ? httParams.append(this.SEARCH_PARAM_NAME, params.searchPhrase) : httParams;
    return httParams;
  }
}
