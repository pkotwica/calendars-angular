package pkotwica.inz.calendars.event.web.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class EditEventResponseFactory extends CalendarsApiResponseFactory {
    private static final String EVENT_DATA_ERR_MESSAGE = "Please fill all data. Pay attention on time of event.";
    private static final String ROOM_BOOKED_MESSAGE = "Room is already booked in given time.";
    private static final String SAVE_EVENT_FAILED = "Save event failed. Contact with support.";
    private static final String DELETE_EVENT_FAILED = "Delete event failed. Contact with support.";


    public static CalendarsApiResponse fillAllDataErrEvent() {
        return new CalendarsApiResponse(false, EVENT_DATA_ERR_MESSAGE);
    }

    public static CalendarsApiResponse roomBooked() {
        return new CalendarsApiResponse(false, ROOM_BOOKED_MESSAGE);
    }

    public static CalendarsApiResponse saveByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, SAVE_EVENT_FAILED);
    }

    public static CalendarsApiResponse deleteByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, DELETE_EVENT_FAILED);
    }
}
