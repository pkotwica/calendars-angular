package pkotwica.inz.calendars.common;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

public class CommonSpecifications {

    public static <E> Specification<E> searchInListOfFields(List<String> attributes, String searchPhrase) {
        String finalText = "%" + searchPhrase + "%";

        return (root, query, builder) -> builder.or(root.getModel().getDeclaredSingularAttributes().stream()
                .filter(a -> attributes.contains(a.getName()))
                .map(a -> builder.like(root.get(a.getName()), finalText))
                .toArray(Predicate[]::new)
        );
    }
}
