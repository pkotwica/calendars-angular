package pkotwica.inz.calendars.initialdata.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import pkotwica.inz.calendars.initialdata.InitialData;

@Component
@Profile("!dev")
public class ProdData implements InitialData {
    private final EssentialData essentialData;

    public ProdData(EssentialData essentialData) {
        this.essentialData = essentialData;
    }

    @Override
    public void load() {
        essentialData.load();
    }
}
