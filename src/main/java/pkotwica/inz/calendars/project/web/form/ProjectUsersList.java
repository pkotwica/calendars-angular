package pkotwica.inz.calendars.project.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProjectUsersList {
    private List<UserData> users;

    public static ProjectUsersList of(List<User> users) {
        List<UserData> usersData = new ArrayList<>();
        users.forEach(user -> usersData.add(new UserData(user)));
        return new ProjectUsersList(usersData);
    }
}
