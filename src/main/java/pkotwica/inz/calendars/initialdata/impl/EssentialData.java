package pkotwica.inz.calendars.initialdata.impl;

import org.springframework.stereotype.Component;
import pkotwica.inz.calendars.event.model.EventType;
import pkotwica.inz.calendars.event.model.EventTypes;
import pkotwica.inz.calendars.event.repo.EventTypeRepository;
import pkotwica.inz.calendars.registration.form.RegisterForm;
import pkotwica.inz.calendars.user.model.Role;
import pkotwica.inz.calendars.user.model.Roles;
import pkotwica.inz.calendars.user.repo.RoleRepository;
import pkotwica.inz.calendars.user.service.UserService;

@Component
public class EssentialData {
    private final EventTypeRepository eventTypeRepository;
    private final RoleRepository roleRepository;
    private final UserService userService;

    public EssentialData(RoleRepository roleRepository, UserService userService, EventTypeRepository eventTypeRepository) {
        this.roleRepository = roleRepository;
        this.userService = userService;
        this.eventTypeRepository = eventTypeRepository;
    }

    void load() {
        addEventsTypes();
        addRoles();
        addAdmin();
    }

    private void addAdmin() {
        userService.addNewUser(new RegisterForm("ADMIN", "ADMIN", "admin", "ADMIN", "ADMIN", "ROLE_ADMIN", "nimda", "nimda"));
    }

    private void addRoles() {
        for (Roles role : Roles.values()) {
            roleRepository.save(Role.builder().name(role.name()).build());
        }
    }

    private void addEventsTypes() {
        for (EventTypes eventType : EventTypes.values()) {
            eventTypeRepository.save(EventType.of(eventType.name()));
        }
    }
}
