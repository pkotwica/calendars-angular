package pkotwica.inz.calendars.event.web.form;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pkotwica.inz.calendars.event.model.Event;

@Getter
@Setter
@NoArgsConstructor
public class EventData {
    private long id;
    private String label;
    private String startDate;
    private String endDate;
    private String type;
    private String room;
    private String ownerName;
    private String ownerEmail;

    private EventData(long id, String label, String startDate, String endDate, String type, String room, String ownerName, String ownerEmail) {
        this.id = id;
        this.label = label;
        this.startDate = startDate;
        this.endDate = endDate;
        this.type = type;
        this.room = room;
        this.ownerName = ownerName;
        this.ownerEmail = ownerEmail;
    }

    public static EventData of(Event event) {
        return new EventData(
                event.getId(),
                event.getLabel(),
                event.getStartDate().toString(),
                event.getEndDate().toString(),
                event.getType().getType(),
                event.getRoom() == null ? "undefined" : event.getRoom().getName(),
                event.getUser().getFirstName() + ' ' + event.getUser().getLastName(),
                event.getUser().getEmail()
        );
    }
}
