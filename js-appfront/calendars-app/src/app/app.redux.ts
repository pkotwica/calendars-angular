import {AuthState} from './redux/interfaces/authorization.interfaces';
import {ActionReducerMap} from '@ngrx/store';
import {SystemRolesDataState, UserDataState, UsersEmailsDataState} from './redux/interfaces/user.interfaces';
import {userDataReducer, usersEmailsReducer} from './redux/reducers/user.reducer';
import {authReducer} from './redux/reducers/authentication.reducer';
import {UserEffects} from './redux/effects/user.effects';
import {AuthEffects} from './redux/effects/auth.effects';
import {SharedEffects} from './redux/effects/shared.effects';
import {
  CalendarDataState,
  EventsTypesDataState,
  OwnedProjectsDataState,
  RoomsDataState
} from './redux/interfaces/calendar.interfaces';
import {calendarDataReducer} from './redux/reducers/calendar-data.reducer';
import {CalendarEffects} from './redux/effects/calendar.effects';
import {eventDataReducer} from './redux/reducers/event.reducer';
import {EventEffects} from './redux/effects/event.effects';
import {roomsDataReducer} from './redux/reducers/room.reducer';
import {RoomEffects} from './redux/effects/room.effects';
import {ownedProjectsDataReducer} from './redux/reducers/projects.reducer';
import {ProjectsEffects} from './redux/effects/projects.effects';
import {systemRolesDataReducer} from './redux/reducers/roles.reducer';
import {RolesEffects} from './redux/effects/roles.effects';
import {AdminState} from './redux/interfaces/admin.interface';
import {adminsStateReducer} from './redux/reducers/admin.reducer';
import {AdminEffects} from './redux/effects/admin.effects';


export interface State {
  auth: AuthState,
  userData: UserDataState,
  calendarData: CalendarDataState,
  eventsTypes: EventsTypesDataState,
  roomsData: RoomsDataState,
  usersEmails: UsersEmailsDataState,
  ownedProjects: OwnedProjectsDataState,
  systemRoles: SystemRolesDataState,
  adminsState?: AdminState,
}

export const reducers: ActionReducerMap<State> = {
  auth: authReducer,
  userData: userDataReducer,
  calendarData: calendarDataReducer,
  eventsTypes: eventDataReducer,
  roomsData: roomsDataReducer,
  usersEmails: usersEmailsReducer,
  ownedProjects: ownedProjectsDataReducer,
  systemRoles: systemRolesDataReducer,
  adminsState: adminsStateReducer,
};

export const effects = [
  UserEffects,
  AuthEffects,
  CalendarEffects,
  SharedEffects,
  EventEffects,
  RoomEffects,
  ProjectsEffects,
  RolesEffects,
  AdminEffects,
];
