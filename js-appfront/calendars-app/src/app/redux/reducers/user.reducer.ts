import {UserActions, UserActionsTypes} from '../actions/user.actions';
import {UserDataState, UsersEmailsDataState} from '../interfaces/user.interfaces';
import {initialLoadingStatus} from '../interfaces/common.interfaces';

const initialUserDataState: UserDataState = {
  status: initialLoadingStatus,
};

export const userDataReducer = (state: UserDataState = initialUserDataState, action: UserActions) => {
  switch (action.type) {
    case UserActionsTypes.GetUserData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case UserActionsTypes.GetUserDataSuccess: {
      return {
        ...state,
        login: action.payload.login,
        firstName: action.payload.firstName,
        lastName: action.payload.lastName,
        email: action.payload.email,
        jobPosition: action.payload.jobPosition,
        role: action.payload.role,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case UserActionsTypes.GetUserDataFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    case UserActionsTypes.ResetUserData: {
      return initialUserDataState;
    }
    default: {
      return state;
    }
  }
};

const initialUsersEmailsDataState: UsersEmailsDataState = {
  status: initialLoadingStatus,
};

export const usersEmailsReducer = (state: UsersEmailsDataState = initialUsersEmailsDataState, action: UserActions) => {
  switch (action.type) {
    case UserActionsTypes.GetUsersEmailsData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case UserActionsTypes.GetUsersEmailsDataSuccess: {
      return {
        ...state,
        data: action.payload.data,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case UserActionsTypes.GetUsersEmailsDataFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state
    }
  }
};
