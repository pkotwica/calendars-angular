package pkotwica.inz.calendars.web.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CalendarsApiResponse {
    private boolean isSuccess;
    private String messageInfo;
}
