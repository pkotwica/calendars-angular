package pkotwica.inz.calendars.event.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "event_types")
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EventType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String type;

    private EventType(String type) {
        this.type = type;
    }

    public static EventType of(String type) {
        return new EventType(type);
    }
}
