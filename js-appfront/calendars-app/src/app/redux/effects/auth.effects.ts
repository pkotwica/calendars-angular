import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map, switchMap, tap} from 'rxjs/operators';
import {
  Auth,
  AuthActionsTypes,
  AuthLoginFailure,
  AuthLoginSuccess,
  AuthLogoutSuccess,
  AutoAuth
} from '../actions/auth.actions';
import {AuthService} from '../../auth/auth.service';
import {ResetAllStates} from '../actions/shared.actions';
import {State} from '../../app.redux';
import {Store} from '@ngrx/store';
import {of} from 'rxjs';
import {GetEventsTypes} from '../actions/event.actions';
import {GetRooms} from '../actions/room.actions';
import {GetUserData, GetUsersEmailsData} from '../actions/user.actions';
import {GetRoles} from '../actions/roles.actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private authService: AuthService,
  ) {
  }

  @Effect()
  auth$ = this.actions$.pipe(
    ofType(AuthActionsTypes.Auth),
    exhaustMap((action: Auth) => this.authService.getToken(action.payload).pipe(
      tap(token => this.authService.login(token)),
      map(token => new AuthLoginSuccess(token)),
      catchError(error => of(new AuthLoginFailure(this.authService.resolveErrorMessage(error.status))))
    ))
  );

  @Effect()
  authSuccess$ = this.actions$.pipe(
    ofType(AuthActionsTypes.AuthSuccess),
    switchMap(() => [
      new GetEventsTypes(),
      new GetRooms(),
      new GetUsersEmailsData(),
      new GetUserData(),
      new GetRoles(),
    ]),
  );

  @Effect()
  autoAuth$ = this.actions$.pipe(
    ofType(AuthActionsTypes.AutoAuth),
    tap((token: AutoAuth) => this.authService.login(token.payload)),
    map((token: AutoAuth) => new AuthLoginSuccess(token.payload)),
  );

  @Effect()
  authLogout$ = this.actions$.pipe(
    ofType(AuthActionsTypes.AuthLogout),
    tap(() => this.authService.logout()),
    map(() => new AuthLogoutSuccess())
  );

  @Effect()
  authLogoutSuccess$ = this.actions$.pipe(
    ofType(AuthActionsTypes.AuthLogoutSuccess),
    map(() => new ResetAllStates())
  );
}
