import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {
  CalendarData,
  EventsTypesDataState,
  GetCalendarDetails, OwnedProjectsDataState,
  RoomsDataState
} from '../interfaces/calendar.interfaces';
import {Observable} from 'rxjs';
import {StandardCalendarsApiResponse} from '../../shared/calendars.model';
import {SaveEventModel} from '../../shared/calendar/event.model';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getMyCalendar(details: GetCalendarDetails): Observable<CalendarData> {
    return this.http.get<CalendarData>(`${environment.calendarsApiUrl}/api/my-calendar${this.preparePathDetails(details)}`);
  }

  getUserCalendar(details: GetCalendarDetails): Observable<CalendarData> {
    const params = `${this.preparePathDetails(details)}?email=${details.userEmail}`;
    return this.http.get<CalendarData>(`${environment.calendarsApiUrl}/api/users-calendar${params}`);
  }

  getProjectCalendar(details: GetCalendarDetails): Observable<CalendarData> {
    const params = `${this.preparePathDetails(details)}?project=${details.projectName}`;
    return this.http.get<CalendarData>(`${environment.calendarsApiUrl}/api/projects-calendar${params}`);
  }

  preparePathDetails(details: GetCalendarDetails): string {
    if(details && details.monthNumber) {
      return details.year ? `/${details.monthNumber}/${details.year}` : `/${details.monthNumber}`;
    }
    return '';
  }

  getEventsTypes(): Observable<EventsTypesDataState> {
    return this.http.get<EventsTypesDataState>(`${environment.calendarsApiUrl}/api/event/types`);
  }

  getRooms(): Observable<RoomsDataState> {
    return this.http.get<RoomsDataState>(`${environment.calendarsApiUrl}/api/rooms-info`);
  }

  getOwnedProjects(): Observable<OwnedProjectsDataState> {
    return this.http.get<OwnedProjectsDataState>(`${environment.calendarsApiUrl}/api/projects`);
  }

  getProjectUsers(projectName: string): Observable<OwnedProjectsDataState> {
    const params = new HttpParams().append('project', projectName);
    return this.http.get<OwnedProjectsDataState>(`${environment.calendarsApiUrl}/api/projects/users`, {params});
  }

  getRoomCalendar(details: GetCalendarDetails): Observable<CalendarData> {
    const params = `${this.preparePathDetails(details)}?room=${details.roomName}`;
    return this.http.get<CalendarData>(`${environment.calendarsApiUrl}/api/rooms-calendar${params}`);
  }

  saveEvent(saveEventModel: SaveEventModel) {
    const path = saveEventModel.id === '' ? 'new' : 'edit';
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/event/${path}`, this.buildSaveEventBody(saveEventModel));
  }

  deleteEvent(eventId: number) {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/event/delete`, {eventId});
  }

  private buildSaveEventBody(data: SaveEventModel) {
    const startTimeUnits = data.form.startTime.split(':');
    const endTimeUnits = data.form.endTime.split(':');

    return {
      eventId: data.id === '' ? undefined : +data.id,
      eventType: data.form.eventType,
      eventLabel: data.form.eventLabel,
      roomName: data.form.room === 'undefined' ? '' : data.form.room,
      year: data.year,
      month: data.month,
      day: data.day,
      startHour: +startTimeUnits[0],
      startMinute: +startTimeUnits[1],
      endHour: +endTimeUnits[0],
      endMinute: +endTimeUnits[1],
    };
  }
}
