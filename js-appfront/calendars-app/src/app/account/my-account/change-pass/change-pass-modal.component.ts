import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {of} from 'rxjs';
import {NgForm} from '@angular/forms';
import {catchError} from 'rxjs/operators';
import {UserService} from '../../../redux/services/user.service';

@Component({
  selector: 'app-change-pass-modal',
  templateUrl: 'change-pass-modal.component.html',
})
export class ChangePassModalComponent {
  changeLoginModalTitle = 'Change password';
  errorMessage;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('changePassForm')
  changePassForm: NgForm;
  matchRepeatPass: boolean = false;
  repeatPassInputBlur: boolean = false;

  constructor(
    private userService: UserService,
  ) {
  }

  submitForm() {
    if (this.changePassForm.valid && this.matchRepeatPass) {
      const formValue = this.changePassForm.value;
      this.userService.changeUserPass(formValue.pass, formValue.passRepeat).pipe(
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe(submitInfo => {
        if (submitInfo.success) {
          this.errorMessage = null;
          this.changePassForm.reset();
          this.modalClose.emit();
        } else {
          this.errorMessage = submitInfo.messageInfo;
        }
      });
    }
  }

  checkMatchPass() {
    this.matchRepeatPass = this.changePassForm.value.pass === this.changePassForm.value.passRepeat;
  }
}
