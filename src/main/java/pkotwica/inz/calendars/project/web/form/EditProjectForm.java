package pkotwica.inz.calendars.project.web.form;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EditProjectForm {
    @NotNull
    @NotEmpty
    private String projectName;

    private String oldProjectName;
}
