import {Action} from '@ngrx/store';
import {AuthState, LoginForm} from '../interfaces/authorization.interfaces';

export enum AuthActionsTypes {
  Auth = '[Auth] login',
  AutoAuth = '[Auth] auto login',
  AuthSuccess = '[Auth] login Success',
  AuthFailure = '[Auth] login Failure',
  AuthLogout = '[Auth] Logout',
  AuthLogoutSuccess = '[Auth] Logout Success',
}

export class Auth implements Action {
  readonly type = AuthActionsTypes.Auth;
  constructor(public payload: LoginForm) {
  }
}

export class AutoAuth implements Action {
  readonly type = AuthActionsTypes.AutoAuth;
  constructor(public payload: AuthState) {
  }
}

export class AuthLoginSuccess implements Action {
  readonly type = AuthActionsTypes.AuthSuccess;

  constructor(public payload: AuthState) {
  }
}

export class AuthLoginFailure implements Action {
  readonly type = AuthActionsTypes.AuthFailure;

  constructor(public payload: string) {
  }
}

export class AuthLogout implements Action {
  readonly type = AuthActionsTypes.AuthLogout;

  constructor(public payload: string) {
  }
}

export class AuthLogoutSuccess implements Action {
  readonly type = AuthActionsTypes.AuthLogoutSuccess;
}

export type AuthActions = Auth | AutoAuth | AuthLoginSuccess | AuthLoginFailure | AuthLogout | AuthLogoutSuccess;
