import {Data} from './common.interfaces';
import {UserDataState} from './user.interfaces';
import {RoomData} from './calendar.interfaces';
import {ProjectInfo} from './admin.interface';

export interface PageableAdministrateUsersData extends PageableData {
  data?: UserDataState[];
}

export interface PageableAdministrateRoomsData extends PageableData {
  data?: RoomData[];
}

export interface PageableAdministrateProjectsData extends PageableData {
  data?: ProjectInfo[];
}

export interface PageableData extends Data {
  currentPage?: number;
  totalPages?: number;
}

export interface PageableQueryInfo {
  page: number;
  size?: number;
  searchPhrase?: string;
}
