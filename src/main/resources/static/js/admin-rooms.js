import {callPostToCalendarsApi} from './common/utils';

let editingRoom = undefined;
let removingRoomName = undefined;

export const bindAdminRooms = () => {
    bindAddNewRoom();
    bindEditRoom();
    bindRemoveRoom();
};

const bindAddNewRoom = () => {
    const addNewRoomButton = document.getElementById('createNewRoomModalButton');

    if (addNewRoomButton) {
        addNewRoomButton.addEventListener('click', function () {
            const body = {
                roomName: document.getElementById('roomNameInput').value,
                roomNumber: document.getElementById('roomNumberInput').value,
            };
            callPostToCalendarsApi('/admin-rooms/add', body, document.getElementById('addNewRoomMessage'));
        });
    }
};

const bindEditRoom = () => {
    const editButtons = document.getElementsByClassName('js-edit-room');
    for (let button of editButtons) {
        button.addEventListener('click', function () {
            editingRoom = button.dataset;
            document.getElementById('roomNameInput-edit').value = editingRoom.name;
            document.getElementById('roomNumberInput-edit').value = editingRoom.number;
            $('#editRoomModal').modal('toggle');
        })
    }

    const saveRoomButton = document.getElementById('editRoomModalButton');
    if (saveRoomButton) {
        saveRoomButton.addEventListener('click', function () {
            const body = {
                oldName: editingRoom.name,
                roomName: document.getElementById('roomNameInput-edit').value,
                roomNumber: document.getElementById('roomNumberInput-edit').value,
            };
            callPostToCalendarsApi('/admin-rooms/edit', body, document.getElementById('editRoomMessage'));
        });
    }
};

const bindRemoveRoom = () => {
    const tryRemoveButtons = document.getElementsByClassName('js-remove-room');
    for (let button of tryRemoveButtons) {
        button.addEventListener('click', function () {
            removingRoomName = button.dataset.name;
            document.getElementById('removingRoomName').value = removingRoomName;
            $('#removeRoomModal').modal('toggle');
        })
    }

    const removeRoomButton = document.getElementById('removeRoomModalButton');
    if (removeRoomButton) {
        removeRoomButton.addEventListener('click', function () {
            callPostToCalendarsApi('/admin-rooms/remove?room=' + removingRoomName, undefined, document.getElementById('removeRoomModalMessage'));
        });
    }
};