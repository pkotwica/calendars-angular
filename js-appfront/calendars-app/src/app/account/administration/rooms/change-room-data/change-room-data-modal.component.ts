import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {RoomData} from '../../../../redux/interfaces/calendar.interfaces';
import {NgForm} from '@angular/forms';
import {AdminService} from '../../../../redux/services/admin.service';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {catchError, tap} from 'rxjs/operators';
import {GetAdminRooms} from '../../../../redux/actions/admin.actions';
import {of} from 'rxjs';

@Component({
  selector: 'app-change-room-data-modal',
  templateUrl: 'change-room-data-modal.component.html',
  styleUrls: ['change-room-data-modal.component.scss'],
})
export class ChangeRoomDataModalComponent {
  changeRoomDataModalTitle = 'Edit room';
  errorMessage;

  @Input()
  room: RoomData;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('addNewRoomForm')
  addNewRoomForm: NgForm;

  constructor(
    private adminService: AdminService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.addNewRoomForm.valid) {
      this.adminService.saveRoomData(this.addNewRoomForm.value, this.room.name).pipe(
        tap(submitInfo => {
          this.onSuccessResponse(submitInfo)
        }),
        catchError(error => {
          this.errorMessage = 'An error occurred. Please try again later.';
          return of({success: false, messageInfo: 'An error occurred. Please try again later.'
          })
        }),
      ).subscribe();
    }
  }

  onDeleteRoom() {
    this.adminService.removeRoom(this.room.name).pipe(
      tap(submitInfo => {
        this.onSuccessResponse(submitInfo)
      }),
      catchError(error => {
        this.errorMessage = 'An error occurred. Please try again later.';
        return of({success: false, messageInfo: 'An error occurred. Please try again later.'})
      }),
    ).subscribe();
  }

  private onSuccessResponse(response) {
    if (response.success) {
      this.errorMessage = null;
      this.state.dispatch(new GetAdminRooms({page: 1}));
      this.addNewRoomForm.reset();
      this.modalClose.emit();
    } else {
      this.errorMessage = response.messageInfo;
    }
  }
}
