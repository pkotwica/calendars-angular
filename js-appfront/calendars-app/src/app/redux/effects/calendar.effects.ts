import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {CalendarService} from '../services/calendar.service';
import {
  CalendarActionsTypes,
  GetCalendarDataFailure,
  GetCalendarDataSuccess,
  GetMyCalendarData,
  GetProjectsCalendarData,
  GetRoomsCalendarData,
  GetUsersCalendarData
} from '../actions/calendar.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {of} from 'rxjs';

@Injectable()
export class CalendarEffects {

  @Effect()
  getMyCalendarData = this.actions$.pipe(
    ofType(CalendarActionsTypes.GetMyCalendarData),
    exhaustMap((action: GetMyCalendarData) => this.calendarService.getMyCalendar(action.payload).pipe(
      map(calendarData => new GetCalendarDataSuccess(calendarData)),
      catchError(() => of(new GetCalendarDataFailure())),
    ))
  );

  @Effect()
  getUsersCalendarData = this.actions$.pipe(
    ofType(CalendarActionsTypes.GetUsersCalendarData),
    exhaustMap((action: GetUsersCalendarData) => this.calendarService.getUserCalendar(action.payload).pipe(
      map(calendarData => new GetCalendarDataSuccess(calendarData)),
      catchError(() => of(new GetCalendarDataFailure())),
    ))
  );

  @Effect()
  getProjectsCalendarData = this.actions$.pipe(
    ofType(CalendarActionsTypes.GetProjectsCalendarData),
    exhaustMap((action: GetProjectsCalendarData) => this.calendarService.getProjectCalendar(action.payload).pipe(
      map(calendarData => new GetCalendarDataSuccess(calendarData)),
      catchError(() => of(new GetCalendarDataFailure())),
    ))
  );

  @Effect()
  getRoomsCalendarData = this.actions$.pipe(
    ofType(CalendarActionsTypes.GetRoomsCalendarData),
    exhaustMap((action: GetRoomsCalendarData) => this.calendarService.getRoomCalendar(action.payload).pipe(
      map(calendarData => new GetCalendarDataSuccess(calendarData)),
      catchError(() => of(new GetCalendarDataFailure())),
    ))
  );

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private calendarService: CalendarService,
  ) {
  }
}
