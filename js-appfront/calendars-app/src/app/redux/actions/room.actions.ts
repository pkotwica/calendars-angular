import {Action} from '@ngrx/store';
import {RoomsDataState} from '../interfaces/calendar.interfaces';

export enum RoomsActionsTypes {
  GetRooms = '[Room] get',
  GetRoomsSuccess = '[Room] get success',
  GetRoomsFailure = '[Room] get failure',
}

export class GetRooms implements Action {
  readonly type = RoomsActionsTypes.GetRooms;
}

export class GetRoomsSuccess implements Action {
  readonly type = RoomsActionsTypes.GetRoomsSuccess;

  constructor(public payload: RoomsDataState) {
  }
}

export class GetRoomsFailure implements Action {
  readonly type = RoomsActionsTypes.GetRoomsFailure;
}

export type RoomsActions = GetRooms | GetRoomsSuccess | GetRoomsFailure;
