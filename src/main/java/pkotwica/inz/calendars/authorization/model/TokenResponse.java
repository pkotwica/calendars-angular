package pkotwica.inz.calendars.authorization.model;

import lombok.Getter;

@Getter
public class TokenResponse {
    private String tokenCode;
    private String expirationDate;

    private TokenResponse(String tokenCode, String expirationDate) {
        this.tokenCode = tokenCode;
        this.expirationDate = expirationDate;
    }

    public static TokenResponse of(AuthToken authToken) {
        return new TokenResponse(authToken.getTokenCode(), authToken.getExpirationDate().toString());
    }

    public static TokenResponse empty() {
        return new TokenResponse("", "");
    }
}
