import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SystemRolesDataState, UserDataState, UsersEmailsDataState} from '../interfaces/user.interfaces';
import {environment} from '../../../environments/environment';
import {StandardCalendarsApiResponse} from '../../shared/calendars.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
  ) {
  }

  getUserData(): Observable<UserDataState> {
    return this.http.get<UserDataState>(`${environment.calendarsApiUrl}/api/user`);
  }

  changeUserLogin(newLogin): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/user/change/login`, {newLogin});
  }

  changeUserPass(newPass, newPassRep): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/user/change/pass`, {
      newPass,
      newPassRep
    });
  }

  getUsersEmails(): Observable<UsersEmailsDataState> {
    return this.http.get<UsersEmailsDataState>(`${environment.calendarsApiUrl}/api/users`);
  }

  saveNewUser(registerForm): Observable<StandardCalendarsApiResponse> {
    return this.http.post<StandardCalendarsApiResponse>(`${environment.calendarsApiUrl}/api/register`, this.parseFromToBody(registerForm));
  }

  private parseFromToBody(registerForm) {
    return {
      firstName: registerForm.firstName,
      lastName: registerForm.lastName,
      login: registerForm.login,
      jobPosition: registerForm.jobPosition,
      email: registerForm.email,
      role: registerForm.role,
      password: registerForm.pass,
      passwordRep: registerForm.passRepetition,
    };
  }

  getSystemRoles(): Observable<SystemRolesDataState> {
    return this.http.get<SystemRolesDataState>(`${environment.calendarsApiUrl}/api/users/roles`);
  }
}
