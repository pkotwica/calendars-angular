package pkotwica.inz.calendars.event.web.form;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum Days {
    MONDAY("Monday", 1),
    TUESDAY("Tuesday", 2),
    WEDNESDAY("Wednesday", 3),
    THURSDAY("Thursday", 4),
    FRIDAY("Friday", 5),
    SATURDAY("Saturday", 6),
    SUNDAY("Sunday", 7);

    private String name;
    private Integer number;

    Days(String name, Integer number) {
        this.name = name;
        this.number = number;
    }

    public static String getNameByNumber(Integer numb) {
        return Arrays.stream(Days.values()).filter(d -> d.getNumber().equals(numb)).findFirst().orElse(Days.MONDAY).getName();
    }
}
