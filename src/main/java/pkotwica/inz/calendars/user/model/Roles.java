package pkotwica.inz.calendars.user.model;

public enum Roles {
    ROLE_ADMIN, ROLE_MODERATOR, ROLE_USER
}
