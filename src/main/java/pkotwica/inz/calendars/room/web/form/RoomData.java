package pkotwica.inz.calendars.room.web.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pkotwica.inz.calendars.room.model.Room;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class RoomData {
    private String name;
    private String number;

    public static RoomData of(Room room) {
        return new RoomData(room.getName(), room.getNumber());
    }
}
