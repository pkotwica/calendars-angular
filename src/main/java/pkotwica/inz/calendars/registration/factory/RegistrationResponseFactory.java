package pkotwica.inz.calendars.registration.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import java.util.Collection;

public class RegistrationResponseFactory extends CalendarsApiResponseFactory {
    private final static String EMAIL_ALERT = "EMAIL_ERR";
    private final static String LOGIN_ALERT = "LOGIN_ERR";

    private static final String EDIT_USER_DATA_FAIL_MESSAGE = "Some data may occurs in database. Pleas try again with proper data.";
    private static final String PASSWORD_NOT_MATCH_MESSAGE = "Password not match.";
    private static final String REMOVE_USER_FAILED_MESSAGE = "Remove user failed. Please contact with support.";
    private static final String EMAIL_EXISTS_FAILED_MESSAGE = "Account with given email already exists.";
    private static final String LOGIN_EXISTS_FAILED_MESSAGE = "Given login is already taken.";

    public static CalendarsApiResponse ediByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, EDIT_USER_DATA_FAIL_MESSAGE);
    }

    public static CalendarsApiResponse passByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, PASSWORD_NOT_MATCH_MESSAGE);
    }

    public static CalendarsApiResponse removeByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, REMOVE_USER_FAILED_MESSAGE);
    }

    public static CalendarsApiResponse byErrList(Collection<String> errList) {
        if(errList.isEmpty()) {
            return success();
        }
        String errMessage = "";

        errMessage = errList.contains(EMAIL_ALERT) ? errMessage + "\n" + EMAIL_EXISTS_FAILED_MESSAGE : errMessage;
        errMessage = errList.contains(LOGIN_ALERT) ? errMessage + "\n" + LOGIN_EXISTS_FAILED_MESSAGE : errMessage;

        return failWithMessage(errMessage);
    }
}
