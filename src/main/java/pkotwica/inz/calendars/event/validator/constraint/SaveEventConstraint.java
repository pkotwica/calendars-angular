package pkotwica.inz.calendars.event.validator.constraint;

import pkotwica.inz.calendars.event.validator.SaveEventValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = SaveEventValidator.class)
@Target( ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SaveEventConstraint {
    String message() default "Dates are invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
