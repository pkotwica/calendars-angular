import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {RoomsDataState} from '../interfaces/calendar.interfaces';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {CalendarService} from '../services/calendar.service';
import {GetRoomsFailure, GetRoomsSuccess, RoomsActionsTypes} from '../actions/room.actions';

@Injectable()
export class RoomEffects {

  @Effect()
  getRooms$ = this.actions$.pipe(
    ofType(RoomsActionsTypes.GetRooms),
    exhaustMap(() => this.calendarService.getRooms().pipe(
      map((rooms: RoomsDataState) => new GetRoomsSuccess(rooms)),
      catchError(() => of(new GetRoomsFailure())),
    )),
  );

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private calendarService: CalendarService,
  ) {
  }
}
