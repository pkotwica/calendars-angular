package pkotwica.inz.calendars.authorization.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "authTokens")
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
public class AuthToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String tokenCode;
    private LocalDateTime expirationDate;

    private AuthToken(String tokenCode, LocalDateTime expirationDate) {
        this.tokenCode = tokenCode;
        this.expirationDate = expirationDate;
    }

    public static AuthToken of(String tokenCode, LocalDateTime expirationDate) {
        return new AuthToken(tokenCode, expirationDate);
    }
}
