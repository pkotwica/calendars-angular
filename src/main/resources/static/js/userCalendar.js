import {getSelectedOption} from './common/utils';

export const bindUserCalendarSelector = () => {
    const userCalendarSelector = document.getElementById('userSelectUserCalendar');

    if (userCalendarSelector) {
        userCalendarSelector.addEventListener('change', function () {
            getUserCalendar(userCalendarSelector);
        })
    }
};

const getUserCalendar = (selector) => {
    const selectedValue = selector.options[selector.selectedIndex].value;

    if (selectedValue !== 'undefined') {
        window.location.replace('/users-calendars?email=' + getSelectedOption(selector));
    }
};