import {Component, OnInit} from '@angular/core';
import {GetCalendarDetails} from '../../redux/interfaces/calendar.interfaces';
import {selectCalendarState, selectOwnedProjects, selectProjectUsers} from '../../redux/selectors/calendar.selector';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {SelectorOptionModel} from '../../shared/selector/selector-data.model';
import {GetOwnedProjects, GetProjectUsers} from '../../redux/actions/projects.actions';
import {GetProjectsCalendarData} from '../../redux/actions/calendar.actions';

@Component({
  selector: 'app-projects-calendars',
  templateUrl: 'projects-calendars.component.html',
  styleUrls: ['projects-calendars.component.scss'],
})
export class ProjectsCalendarsComponent implements OnInit {
  ownedProjects$ = this.state.select(selectOwnedProjects);
  projectUsers$ = this.state.select(selectProjectUsers);
  calendarData$ = this.state.select(selectCalendarState);
  monthNumber: number;
  year: number;
  selectedProject: string;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.monthNumber = this.route.snapshot.params['monthNumber'];
    this.year = this.route.snapshot.params['year'];
    this.selectedProject = this.route.snapshot.queryParams['project'];
    this.getProjectsCalendarData();
    this.getListOfProjects();
    this.getProjectUsers();

    this.route.params.subscribe((params: Params) => {
      this.monthNumber = params['monthNumber'];
      this.year = params['year'];
      this.getProjectsCalendarData();
    });

    this.route.queryParams.subscribe((params: Params) => {
      this.selectedProject = params['project'];
      this.getProjectsCalendarData();
      this.getProjectUsers();
    });
  }

  getProjectsCalendarData() {
    if (this.monthNumber > 12 || this.monthNumber < 1) {
      this.router.navigate(['/not-found/month']);
    } else {
      this.dispatchProjectsCalendarData();
    }
  }

  private getListOfProjects() {
    this.state.dispatch(new GetOwnedProjects());
  }

  private getProjectUsers() {
    if (this.selectedProject) {
      this.state.dispatch(new GetProjectUsers(this.selectedProject));
    }
  }

  onChoseProject(project: string) {
    let urlPath = '';
    if (this.monthNumber) {
      urlPath = `/${this.monthNumber}${this.year ? '/' + this.year : ''}`
    }

    this.router.navigate(
      ['/projects-calendars' + urlPath],
      {queryParams: {project: project}}
    );
  }

  onChangeMonth(event: GetCalendarDetails) {
    this.router.navigate(['/projects-calendars', event.monthNumber, event.year], {queryParams: {project: this.selectedProject}});
  }

  dispatchProjectsCalendarData() {
    if (this.selectedProject) {
      this.state.dispatch(new GetProjectsCalendarData({
        monthNumber: this.monthNumber,
        year: this.year,
        projectName: this.selectedProject,
      }));
    }
  }

  parseSelectorData(ownedProjects: string[]) {
    const usersEmailsSelect: SelectorOptionModel[] = [];

    if (ownedProjects) {
      ownedProjects.forEach(projectName => usersEmailsSelect.push({value: projectName, name: projectName}));
    }

    return {values: usersEmailsSelect}
  }
}
