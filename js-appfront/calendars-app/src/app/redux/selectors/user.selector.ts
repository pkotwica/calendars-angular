import {State} from '../../app.redux';
import {createSelector} from '@ngrx/store';
import {SystemRolesDataState, UserDataState, UsersEmailsDataState} from '../interfaces/user.interfaces';

export const selectUserDataState = (globalState: State) => globalState.userData;

export const selectUserData = createSelector(selectUserDataState, (state: UserDataState): UserDataState => state);
export const selectCurrentUserEmail = createSelector(selectUserDataState, (state: UserDataState): string => state.email);
export const selectCurrentUserRole = createSelector(selectUserDataState, (state: UserDataState): string => state.role);

export const selectUsersEmailsDataState = (globalState: State) => globalState.usersEmails;

export const selectUsersEmailsData = createSelector(selectUsersEmailsDataState, (state: UsersEmailsDataState): UsersEmailsDataState => state);

export const selectSystemRoles = (globalState: State) => globalState.systemRoles;

export const selectSystemRolesData = createSelector(selectSystemRoles, (state: SystemRolesDataState): SystemRolesDataState => state);

