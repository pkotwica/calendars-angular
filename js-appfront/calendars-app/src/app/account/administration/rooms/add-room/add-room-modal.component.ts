import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {selectSystemRolesData} from '../../../../redux/selectors/user.selector';
import {UserDataState} from '../../../../redux/interfaces/user.interfaces';
import {NgForm} from '@angular/forms';
import {AdminService} from '../../../../redux/services/admin.service';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {catchError, tap} from 'rxjs/operators';
import {GetAdminRooms, GetUsers} from '../../../../redux/actions/admin.actions';
import {of} from 'rxjs';
import {RoomData} from '../../../../redux/interfaces/calendar.interfaces';

@Component({
  selector: 'app-add-room-modal',
  templateUrl: 'add-room-modal.component.html',
  styleUrls: ['add-room-modal.component.scss'],
})
export class AddRoomModalComponent {
  addRoomModalTitle = 'Add new room';
  errorMessage;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('addNewRoomForm')
  addNewRoomForm: NgForm;

  constructor(
    private adminService: AdminService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.addNewRoomForm.valid) {
      this.adminService.saveNewRoom(this.addNewRoomForm.value).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            this.errorMessage = null;
            this.state.dispatch(new GetAdminRooms({page: 1}));
            this.addNewRoomForm.reset();
            this.modalClose.emit();
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }
}
