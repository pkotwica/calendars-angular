package pkotwica.inz.calendars.administration.project;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.project.web.form.ProjectData;
import pkotwica.inz.calendars.project.web.form.ProjectInfo;

@Service
public interface AdminProjectFacade {

    Page<ProjectData> findRoomsToAdministrate(int pageNumber, int pageSize, String searchPhrase);

    ProjectInfo getProjectUsers(String projectName);

    boolean addProject(EditProjectForm editProjectForm);

    boolean editProject(EditProjectForm editProjectForm);

    boolean removeProject(EditProjectForm editProjectForm);

    boolean addUserToProject(EditUserInProjectForm editUserInProjectForm);

    boolean removeUserFromProject(EditUserInProjectForm editUserInProjectForm);
}
