import {Component, EventEmitter, Input, Output} from '@angular/core';
import {SelectorDataModel} from './selector-data.model';

@Component({
  selector: 'app-selector',
  templateUrl: 'selector.component.html',
  styleUrls: ['selector.component.scss'],
})
export class SelectorComponent {
  @Input()
  title: string;
  @Input()
  selectorData: SelectorDataModel;
  @Input()
  chosenValue: string;
  @Input()
  isLoading: boolean = false;

  @Output()
  valueChosenEvent = new EventEmitter<string>();

  select(event) {
    this.valueChosenEvent.emit(event.target.value);
  }
}
