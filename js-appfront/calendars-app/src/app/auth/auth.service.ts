import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Store} from '@ngrx/store';
import {State} from '../app.redux';
import {AuthLogout, AutoAuth} from '../redux/actions/auth.actions';
import {AuthState, LoginForm} from '../redux/interfaces/authorization.interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLogged = new BehaviorSubject<boolean>(false);
  error = new Subject<string>();

  tokenLabelLS = 'tokenCode';
  tokenExpirationDateLabelLS = 'tokenExpiryDate';

  enterUrl: string;

  constructor(
    private http: HttpClient,
    private router: Router,
    private state: Store<State>,
  ) {
  }

  getToken(loginForm: LoginForm): Observable<AuthState> {
    return this.http.post<AuthState>(`${environment.calendarsApiUrl}/token`, {
      username: loginForm.username,
      password: loginForm.password
    });
  }

  login(auth: AuthState) {
    this.isLogged.next(true);
    this.saveTokenInLocalStorage(auth);
    this.autoLogout(auth.expirationDate);
    this.redirectToStartPage();
  }

  private redirectToStartPage() {
    if (!this.enterUrl || this.enterUrl === '/login') {
      this.router.navigate(['/']);
    } else {
      const urlParts = this.enterUrl.split('?');
      this.router.navigate([urlParts[0]], {queryParams: this.retrieveQueryParams(urlParts[1])});
    }
    this.enterUrl = '/';
  }

  private retrieveQueryParams(urlParamsString): any {
    let paramsObject = {};

    if(urlParamsString) {
      const paramsString = urlParamsString.split('&');
      paramsString.forEach(paramString => {
        let param = paramString.split('=');
        paramsObject[`${param[0]}`] = param[1];
      });
    }

    return paramsObject
  }

  logout() {
    localStorage.removeItem(this.tokenLabelLS);
    localStorage.removeItem(this.tokenExpirationDateLabelLS);
    this.isLogged.next(false);
    this.router.navigate(['/login']);
  }

  autoLogin(startUrl) {
    this.enterUrl = startUrl;
    const token = localStorage.getItem(this.tokenLabelLS);
    const tokenExpiryDate = localStorage.getItem(this.tokenExpirationDateLabelLS);

    if (this.retrieveToken({tokenCode: token, expirationDate: tokenExpiryDate})) {
      this.state.dispatch(new AutoAuth({tokenCode: token, expirationDate: tokenExpiryDate}));
      this.autoLogout(tokenExpiryDate);
    }
  }

  private saveTokenInLocalStorage(auth: AuthState) {
    localStorage.setItem(this.tokenLabelLS, auth.tokenCode);
    localStorage.setItem(this.tokenExpirationDateLabelLS, auth.expirationDate);
  }

  autoLogout(expiryDate) {
    const timeout = new Date(expiryDate).getTime() - Date.now();

    setTimeout(() => {
      this.state.dispatch(new AuthLogout('Session expired.\nYou have been logout automatically.'));
    }, timeout);
  }

  retrieveToken(tokenData: AuthState): string {
    if (!tokenData.tokenCode || !tokenData.expirationDate) {
      return null
    }

    const expiryDate = new Date(tokenData.expirationDate);
    if (Date.now() > expiryDate.getTime()) {
      return null;
    }
    return tokenData.tokenCode;
  }

  resolveErrorMessage(responseStatus: number) {
    switch(responseStatus) {
      case 401: {
        return 'Wrong credentials';
      }
      default: {
        return 'Unknown error. Please try again later.';
      }
    }
  }
}
