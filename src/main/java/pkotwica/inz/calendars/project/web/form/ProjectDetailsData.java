package pkotwica.inz.calendars.project.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pkotwica.inz.calendars.user.web.form.UserWithEmail;

import java.util.List;

@Getter
@AllArgsConstructor
public class ProjectDetailsData {
    private List<UserProjectData> users;
    private List<UserWithEmail> allUsers;
    private String project;
}
