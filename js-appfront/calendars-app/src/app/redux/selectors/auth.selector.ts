import {AuthState} from '../interfaces/authorization.interfaces';
import {State} from '../../app.redux';
import {createSelector} from '@ngrx/store';
import {LoadingStatus} from '../interfaces/common.interfaces';

export const selectAuth = (globalState: State): AuthState => globalState.auth;

export const selectAuthState = createSelector(selectAuth, (state: AuthState): AuthState => state);
export const selectAuthStatus = createSelector(selectAuth, (state: AuthState): LoadingStatus => state.status);

