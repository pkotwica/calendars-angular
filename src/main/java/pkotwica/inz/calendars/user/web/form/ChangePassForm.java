package pkotwica.inz.calendars.user.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pkotwica.inz.calendars.user.validator.constraint.ChangePasswordConstraint;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ChangePasswordConstraint
public class ChangePassForm {
    @NotNull
    @NotEmpty
    private String newPass;
    @NotNull
    @NotEmpty
    private String newPassRep;
}
