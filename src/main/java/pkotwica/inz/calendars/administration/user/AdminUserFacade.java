package pkotwica.inz.calendars.administration.user;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.registration.form.EditUserDataForm;
import pkotwica.inz.calendars.registration.form.EditUserPasswordForm;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;

@Service
public interface AdminUserFacade {

    Page<UserData> findUsersToAdministrate(int pageNumber, int pageSize, String searchPhrase);

    boolean editUserData(EditUserDataForm editUserDataForm);

    boolean editUserPassword(EditUserPasswordForm editUserPasswordForm);
}
