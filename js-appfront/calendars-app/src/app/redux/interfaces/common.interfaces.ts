export interface Data {
  status?: LoadingStatus;
}

export interface LoadingStatus {
  error: boolean;
  errorMessage?: string;
  loading: boolean;
}

export const initialLoadingStatus = {
  error: false,
  errorMessage: '',
  loading: false,
};
