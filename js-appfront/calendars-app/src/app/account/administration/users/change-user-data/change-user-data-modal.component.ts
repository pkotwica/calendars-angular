import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {selectSystemRolesData} from '../../../../redux/selectors/user.selector';
import {NgForm} from '@angular/forms';
import {UserService} from '../../../../redux/services/user.service';
import {Store} from '@ngrx/store';
import {State} from '../../../../app.redux';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {AdminService} from '../../../../redux/services/admin.service';
import {UserDataState} from '../../../../redux/interfaces/user.interfaces';
import {GetUsers} from '../../../../redux/actions/admin.actions';

@Component({
  selector: 'app-change-user-data-modal',
  templateUrl: 'change-user-data-modal.component.html',
})
export class ChangeUserDataModalComponent {
  registerUserModalTitle = 'Edit User Data';
  errorMessage;
  rolesData$ = this.state.select(selectSystemRolesData);

  @Input()
  user: UserDataState;

  @Output()
  modalClose = new EventEmitter();

  @ViewChild('editUserDataForm')
  editUserDataForm: NgForm;

  constructor(
    private adminService: AdminService,
    private state: Store<State>,
  ) {
  }

  submitForm() {
    if (this.editUserDataForm.valid) {
      this.adminService.saveUserData(this.editUserDataForm.value, this.user.email).pipe(
        tap(submitInfo => {
          if (submitInfo.success) {
            this.errorMessage = null;
            this.state.dispatch(new GetUsers({page: 1}));
            this.editUserDataForm.reset();
            this.modalClose.emit();
          } else {
            this.errorMessage = submitInfo.messageInfo;
          }
        }),
        catchError(error => of({success: false, messageInfo: 'An error occurred. Please try again later.'})),
      ).subscribe();
    }
  }
}
