import {callPostToCalendarsApi} from './common/utils';

export const bindAccountPage = () => {
    bindSaveNewLoginButton();
    bindSaveNewPassButton();
};

const bindSaveNewLoginButton = () => {
    const saveNewLoginButton = document.getElementById('saveLoginModalButton');
    const newLoginInput = document.getElementById('changeLoginInput');

    if (saveNewLoginButton && newLoginInput) {
        saveNewLoginButton.addEventListener('click', () => {
            const body = {newLogin: newLoginInput.value};
            callPostToCalendarsApi('/change-user-data/login', body, document.getElementById('loginChangedMessage'));
        });
    }
};

const bindSaveNewPassButton = () => {
    const saveChangePassButton = document.getElementById('savePassModalButton');
    const newPassInput = document.getElementById('changePassInput');
    const newPassRepInput = document.getElementById('changePassRepInput');

    if (saveChangePassButton && newPassInput) {
        saveChangePassButton.addEventListener('click', () => {
            const body = {
                newPass: newPassInput.value,
                newPassRep: newPassRepInput.value,
            };
            callPostToCalendarsApi('/change-user-data/pass', body, document.getElementById('passChangedMessage'));
        });
    }
};