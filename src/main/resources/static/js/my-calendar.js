import {callPostToCalendarsApi, getSelectedOption} from "./common/utils";

let isEvent = false;
let newEventData = undefined;
let editEventData = undefined;

export const bindMyCalendar = () => {
    const myCalendar = document.querySelector('#calendar.js-my-calendar');

    if (myCalendar) {
        bindEditEventModal('js-my-calendar-event');
        bindAddNewEventModal('js-my-calendar-day');
        bindSaveNewEventButton();
        bindEditEventButton();
        bindDeleteEventButton();
    }
};

export const bindAddNewEventModal = (className) => {
    const calendarDays = document.getElementsByClassName(className);

    for (let day of calendarDays) {
        day.addEventListener('click', function () {
            if (!isEvent) {
                $('#addEventModal').modal('toggle');
                populateAddEventModal(day);
            }
            isEvent = false;
        });
    }
};

export const bindEditEventModal = (className) => {
    const events = document.getElementsByClassName(className);

    for (let event of events) {
        event.addEventListener('click', function () {
            isEvent = true;
            populateEditEventModal(event);
            $('#editEventModal').modal('toggle');
        });
    }
};

const populateAddEventModal = (day) => {
    newEventData = day.dataset;
    document.getElementById('addEventDate').innerHTML = ' - ' + newEventData.day + '.' + newEventData.month + '.' + newEventData.year;
};

const populateEditEventModal = (event) => {
    editEventData = event.dataset;
    document.getElementById('editEventDate').innerHTML = ' - ' + editEventData.day + '.' + editEventData.month + '.' + editEventData.year;
    setOption(document.getElementById('eventTypeSelect-edit'), editEventData.type);
    if (editEventData.room !== undefined) {
        setOption(document.getElementById('roomSelect-edit'), editEventData.room);
    }
    document.getElementById('eventLabelInput-edit').value = editEventData.label;
    setHour(document.getElementById('eventStartHourInput-edit'), document.getElementById('eventStartMinuteInput-edit'), editEventData.start);
    setHour(document.getElementById('eventEndHourInput-edit'), document.getElementById('eventEndMinuteInput-edit'), editEventData.end);
};

export const setOption = (selectElement, value) => {
    if (selectElement) {
        const options = selectElement.options;
        for (let i = 0, optionsLength = options.length; i < optionsLength; i++) {
            if (options[i].value === value) {
                selectElement.selectedIndex = i;
            }
        }
    }
};

const setHour = (hourInput, minuteInput, time) => {
    const times = time.split(':');

    hourInput.value = times[0];
    minuteInput.value = times[1];
};


export const bindSaveNewEventButton = () => {
    const saveButton = document.getElementById('saveEventAddModalButton');

    if (saveButton) {
        saveButton.addEventListener('click', saveNewEvent)
    }
};

const saveNewEvent = () => {
    const room = document.getElementById('roomSelect-new');
    let roomName;
    if (room) {
        roomName = getSelectedOption(room);
    } else {
        roomName = newEventData.room;
    }

    const body = {
        eventType: document.getElementById('eventTypeSelect-new').value,
        eventLabel: document.getElementById('eventLabelInput-new').value,
        roomName: roomName,
        year: newEventData.year,
        month: newEventData.month,
        day: newEventData.day,
        startHour: document.getElementById('eventStartHourInput-new').value,
        startMinute: document.getElementById('eventStartMinuteInput-new').value,
        endHour: document.getElementById('eventEndHourInput-new').value,
        endMinute: document.getElementById('eventEndMinuteInput-new').value,
    };

    callPostToCalendarsApi('/event/new', body, document.getElementById('eventNewMessage'));
};

export const bindEditEventButton = () => {
    const editButton = document.getElementById('saveEventEditModalButton');

    if (editButton) {
        editButton.addEventListener('click', saveEditEvent)
    }
};

const saveEditEvent = () => {
    const room = document.getElementById('roomSelect-edit');
    let roomName;
    if (room) {
        roomName = getSelectedOption(room);
    } else {
        roomName = editEventData.room;
    }

    const body = {
        eventId: editEventData.id,
        eventType: document.getElementById('eventTypeSelect-edit').value,
        roomName: roomName,
        eventLabel: document.getElementById('eventLabelInput-edit').value,
        year: editEventData.year,
        month: editEventData.month,
        day: editEventData.day,
        startHour: document.getElementById('eventStartHourInput-edit').value,
        startMinute: document.getElementById('eventStartMinuteInput-edit').value,
        endHour: document.getElementById('eventEndHourInput-edit').value,
        endMinute: document.getElementById('eventEndMinuteInput-edit').value,
    };

    callPostToCalendarsApi('/event/edit', body, document.getElementById('eventChangedMessage'));
};

export const bindDeleteEventButton = () => {
    const deleteButton = document.getElementById('deleteEventEditModalButton');

    if (deleteButton) {
        deleteButton.addEventListener('click', function () {
            const body = {
                eventId: editEventData.id,
            };
            callPostToCalendarsApi('/event/delete', body, document.getElementById('eventChangedMessage'));
        })
    }
};