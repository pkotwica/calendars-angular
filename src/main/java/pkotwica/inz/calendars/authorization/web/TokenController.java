package pkotwica.inz.calendars.authorization.web;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import pkotwica.inz.calendars.authorization.model.AuthToken;
import pkotwica.inz.calendars.authorization.model.LoginBody;
import pkotwica.inz.calendars.authorization.model.TokenResponse;
import pkotwica.inz.calendars.authorization.service.AuthService;

import java.util.Optional;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.TOKEN_API_PREFIX;

@RestController(TOKEN_API_PREFIX)
public class TokenController {

    private AuthService authService;

    public TokenController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<?> getToken(@RequestBody final LoginBody loginBody) {
        Optional<AuthToken> token = authService.generateAuthToken(loginBody);
        if (!token.isPresent()) {
            return new ResponseEntity<>("Unauthorized. Wrong Credentials.", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(TokenResponse.of(token.get()), HttpStatus.OK);
    }
}
