package pkotwica.inz.calendars.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.project.model.Project;
import pkotwica.inz.calendars.user.model.User;

import java.awt.print.Pageable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findUserById(long id);
    Optional<User> findUserByLogin(String login);
    Optional<User> findUserByEmail(String email);
    List<User> findAllByOrderByLastName();
    List<User> findUsersByLoginOrEmail(String login, String email);

    Optional<User> findByToken_TokenCodeAndToken_ExpirationDateGreaterThan(String token, LocalDateTime date);
}
