import {Data} from './common.interfaces';

export interface UserDataState extends Data {
  login?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  jobPosition?: string;
  role?: string;
}

export interface UsersEmailsDataState extends Data {
  data?: [
    {
      email: string,
      name: string,
    }
  ]
}

export interface SystemRolesDataState extends Data {
  roles?: string[];
}
