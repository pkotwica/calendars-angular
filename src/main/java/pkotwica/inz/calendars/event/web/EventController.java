package pkotwica.inz.calendars.event.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pkotwica.inz.calendars.event.service.EventService;
import pkotwica.inz.calendars.event.web.factory.EditEventResponseFactory;
import pkotwica.inz.calendars.event.web.form.DeleteEventForm;
import pkotwica.inz.calendars.event.web.form.SaveEventForm;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

import javax.validation.Valid;
import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@RestController
@RequestMapping()
public class EventController {

    @AllArgsConstructor
    @Getter
    @Setter
    static class EventList {
        private List<String> eventsTypes;
    }

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(PROTECTED_API_PREFIX + "/event/types")
    public EventList getEventsTypes() {
        return new EventList(eventService.getEventTypes());
    }

    @PostMapping(PROTECTED_API_PREFIX + "/event/new")
    public CalendarsApiResponse saveNewEvent(@Valid @RequestBody SaveEventForm saveEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllDataErrEvent();
        }

        if (eventService.checkIsSpareRoom(saveEventForm)) {
            return EditEventResponseFactory.saveByIsSuccess(eventService.saveNewEvent(saveEventForm));
        } else {
            return EditEventResponseFactory.roomBooked();
        }
    }

    @PostMapping(PROTECTED_API_PREFIX + "/event/edit")
    public CalendarsApiResponse editEvent(@Valid @RequestBody SaveEventForm saveEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllDataErrEvent();
        }

        if (eventService.checkIsSpareRoom(saveEventForm)) {
            return EditEventResponseFactory.saveByIsSuccess(eventService.editEvent(saveEventForm));
        } else {
            return EditEventResponseFactory.roomBooked();
        }
    }

    @PostMapping(PROTECTED_API_PREFIX + "/event/delete")
    public CalendarsApiResponse deleteEvent(@Valid @RequestBody DeleteEventForm deleteEventForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return EditEventResponseFactory.fillAllError();
        }
        return EditEventResponseFactory.deleteByIsSuccess(eventService.deleteEvent(deleteEventForm));
    }
}
