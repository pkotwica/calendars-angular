import {Action} from '@ngrx/store';
import {CalendarData, GetCalendarDetails} from '../interfaces/calendar.interfaces';

export enum CalendarActionsTypes {
  GetMyCalendarData = '[Calendar] get my data',
  GetUsersCalendarData = '[Calendar] get users data',
  GetProjectsCalendarData = '[Calendar] get projects data',
  GetRoomsCalendarData = '[Calendar] get rooms data',
  GetCalendarDataSuccess = '[Calendar] get data success',
  GetCalendarDataFailure = '[Calendar] get data failure',
}

export class GetMyCalendarData implements Action {
  readonly type = CalendarActionsTypes.GetMyCalendarData;

  constructor(public payload: GetCalendarDetails) {
  }
}

export class GetUsersCalendarData implements Action {
  readonly type = CalendarActionsTypes.GetUsersCalendarData;

  constructor(public payload: GetCalendarDetails) {
  }
}

export class GetProjectsCalendarData implements Action {
  readonly type = CalendarActionsTypes.GetProjectsCalendarData;

  constructor(public payload: GetCalendarDetails) {
  }
}

export class GetRoomsCalendarData implements Action {
  readonly type = CalendarActionsTypes.GetRoomsCalendarData;

  constructor(public payload: GetCalendarDetails) {
  }
}

export class GetCalendarDataSuccess implements Action {
  readonly type = CalendarActionsTypes.GetCalendarDataSuccess;

  constructor(public payload: CalendarData) {
  }
}

export class GetCalendarDataFailure implements Action {
  readonly type = CalendarActionsTypes.GetCalendarDataFailure;
}

export type CalendarActions = GetMyCalendarData | GetUsersCalendarData | GetProjectsCalendarData | GetRoomsCalendarData | GetCalendarDataSuccess | GetCalendarDataFailure;
