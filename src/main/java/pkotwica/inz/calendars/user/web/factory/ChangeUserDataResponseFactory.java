package pkotwica.inz.calendars.user.web.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class ChangeUserDataResponseFactory extends CalendarsApiResponseFactory {
    private final static String FAILED_LOGIN_MESSAGE = "Login change failed. Try another login";
    private final static String FAILED_PASS_MESSAGE = "Password repetition do not match";

    public static CalendarsApiResponse changeLoginResponse(boolean success) {
        return byIsSuccess(success, FAILED_LOGIN_MESSAGE);
    }

    public static CalendarsApiResponse changePassResponse(boolean success) {
        return byIsSuccess(success, FAILED_PASS_MESSAGE);
    }
}
