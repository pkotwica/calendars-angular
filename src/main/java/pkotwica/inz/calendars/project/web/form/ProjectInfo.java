package pkotwica.inz.calendars.project.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProjectInfo {
    private String name;
    private List<UserData> members;
    private List<UserData> notMembers;

    public static ProjectInfo of(String projectName, List<User> usersByProject, List<User> restUsers) {
        return new ProjectInfo(
                projectName,
                parseToUserData(usersByProject),
                parseToUserData(restUsers)
        );
    }

    private static List<UserData> parseToUserData(List<User> listOfUsers) {
        return listOfUsers.stream().map(UserData::new).collect(Collectors.toList());
    }
}
