package pkotwica.inz.calendars.event.web.form;

import lombok.*;
import pkotwica.inz.calendars.event.validator.constraint.SaveEventConstraint;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SaveEventConstraint
@Builder
public class SaveEventForm {
    private long eventId;
    @NotNull
    @NotEmpty
    private String eventType;
    @NotNull
    @NotEmpty
    private String eventLabel;
    @NotNull
    private String roomName;
    @NotNull
    @Min(1990)
    @Max(2100)
    private Integer year;
    @NotNull
    @Min(1)
    @Max(12)
    private Integer month;
    @NotNull
    @Min(1)
    @Max(31)
    private Integer day;
    @NotNull
    @Min(0)
    @Max(24)
    private Integer startHour;
    @NotNull
    @Min(0)
    @Max(59)
    private Integer startMinute;
    @NotNull
    @Min(0)
    @Max(23)
    private Integer endHour;
    @NotNull
    @Min(0)
    @Max(59)
    private Integer endMinute;
}
