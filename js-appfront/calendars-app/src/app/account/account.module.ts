import {NgModule} from '@angular/core';
import {AccountComponent} from './account.component';
import {AccountRoutingModule} from './account-routing.module';
import {HeaderComponent} from './header/header.component';
import {MyAccountComponent} from './my-account/my-account.component';
import {MyCalendarComponent} from './my-calendar/my-calendar.component';
import {UsersCalendarsComponent} from './users-calendars/users-calendars.component';
import {ChangeLoginModalComponent} from './my-account/change-login/change-login-modal.component';
import {ChangePassModalComponent} from './my-account/change-pass/change-pass-modal.component';
import {ProjectsCalendarsComponent} from './projects-calendars/projects-calendars.component';
import {RoomsCalendarsComponent} from './rooms-calendars/rooms-calendars.component';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        AccountComponent,
        HeaderComponent,
        MyAccountComponent,
        MyCalendarComponent,
        UsersCalendarsComponent,
        ChangeLoginModalComponent,
        ChangePassModalComponent,
        ProjectsCalendarsComponent,
        RoomsCalendarsComponent,
    ],
    imports: [
        AccountRoutingModule,
        SharedModule,
        FormsModule,
    ],
    exports: [
        ChangePassModalComponent
    ]
})
export class AccountModule {

}
