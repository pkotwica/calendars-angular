import {bindRegisterLoginAutoFill} from './registerPage';
import {bindAccountPage} from './accountPage';
import {bindMyCalendar} from './my-calendar';
import {bindUserCalendarSelector} from './userCalendar';
import {bindAdminProjects} from './admin-projects';
import {bindProjectCalendar} from './project-calendar';
import {bindRoomCalendar} from './room-calendar';
import {bindAdminRooms} from './admin-rooms';
import {bindAdminUser} from './admin-user';

const init = () => {
    bindAccountPage();
    bindRegisterLoginAutoFill();
    bindMyCalendar();
    bindUserCalendarSelector();
    bindAdminProjects();
    bindProjectCalendar();
    bindRoomCalendar();
    bindAdminRooms();
    bindAdminUser();
};

window.addEventListener('load', init);