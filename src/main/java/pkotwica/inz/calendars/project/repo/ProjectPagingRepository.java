package pkotwica.inz.calendars.project.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.project.model.Project;

@Repository
public interface ProjectPagingRepository extends PagingAndSortingRepository<Project, Long> {
    Page<Project> findAllByNameIsContaining(String searchPhrase, Pageable page);
}
