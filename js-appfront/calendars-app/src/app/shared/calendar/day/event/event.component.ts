import {Component, Input} from '@angular/core';
import {EventData} from '../../../../redux/interfaces/calendar.interfaces';

@Component({
  selector: 'app-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.scss'],
})
export class EventComponent {
  @Input()
  eventData: EventData;
  @Input()
  editable: boolean = true;
  @Input()
  currentUserEmail: string;

  isMineEvent() {
    return this.currentUserEmail === this.eventData.ownerEmail;
  }
}
