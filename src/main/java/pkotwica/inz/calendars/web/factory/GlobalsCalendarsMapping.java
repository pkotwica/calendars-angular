package pkotwica.inz.calendars.web.factory;

public class GlobalsCalendarsMapping {
    public static final String PROTECTED_API_PREFIX = "api";
    public static final String TOKEN_API_PREFIX = "token";

    public static String createProtectedUri(String uri) {
        return PROTECTED_API_PREFIX + "/" + uri;
    }
}
