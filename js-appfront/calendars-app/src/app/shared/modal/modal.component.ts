import {Component, EventEmitter, HostListener, Input, Output} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.scss'],
})
export class ModalComponent {

  @Input()
  formId: NgForm;
  @Input()
  modalTitle;
  @Input()
  modalContent;
  @Input()
  formValid = true;
  @Input()
  errorMessage;
  @Input()
  isDeletable: boolean = false;

  @Output()
  formSubmitted = new EventEmitter();
  @Output()
  modalClosed = new EventEmitter();
  @Output()
  deleteClick = new EventEmitter();

  closeModal() {
    this.modalClosed.emit();
  }

  deleteOption() {
    this.deleteClick.emit();
  }

  @HostListener('document:keydown.escape')
  onEscapeClick() {
    this.closeModal();
  }
}
