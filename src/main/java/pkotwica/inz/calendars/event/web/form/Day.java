package pkotwica.inz.calendars.event.web.form;

import lombok.*;
import pkotwica.inz.calendars.event.model.Event;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Builder
public class Day {
    private Integer number;
    private Integer numberOfWeekDay;
    private String name;
    private List<Event> events;
}
