import {EventsTypesDataState} from '../interfaces/calendar.interfaces';
import {EventActions, EventActionsTypes} from '../actions/event.actions';
import {initialLoadingStatus} from '../interfaces/common.interfaces';

const initialEventsTypesDataState: EventsTypesDataState = {
  status: initialLoadingStatus,
};

export const eventDataReducer = (state: EventsTypesDataState = initialEventsTypesDataState, action: EventActions) => {
  switch (action.type) {
    case EventActionsTypes.GetEventsTypes: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case EventActionsTypes.GetEventsTypesSuccess: {
      return {
        ...state,
        eventsTypes: action.payload.eventsTypes,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case EventActionsTypes.GetEventsTypesFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }
};
