import {Data} from './common.interfaces';
import {UserDataState} from './user.interfaces';

export interface CalendarDataState extends Data {
  calendarData?: CalendarData;
}

export interface CalendarData {
  currentMonth?: MonthData;
  prevMonth?: MonthData;
  nextMonth?: MonthData;
  currentYear?: number;
  prevYear?: number;
  nextYear?: number;
  startEmptyDays?: number;
  endEmptyDays?: number;
  days?: DayData[];
}

export interface MonthData {
  name?: string;
  number?: number;
}

export interface DayData {
  empty?: boolean;
  number: number;
  numberOfWeekDay: number;
  name: string;
  events: EventData[];
}

export interface EventData {
  id: number;
  label: string;
  startDate: string;
  endDate: string;
  type: string;
  room: string;
  ownerName: string;
  ownerEmail: string;
}

export interface GetCalendarDetails {
  monthNumber: number;
  year: number;
  userEmail?: string;
  roomName?: string;
  projectName?: string;
}

export interface EventsTypesDataState extends Data {
  eventsTypes?: string[];
}

export interface EventsTypesData {
  types: string[];
}

export interface RoomsDataState extends Data {
  rooms?: RoomData[];
}

export interface RoomData {
  name: string;
  number: string;
}

export interface OwnedProjectsDataState extends Data {
  projects?: string[];
  users?: UserDataState[];
}
