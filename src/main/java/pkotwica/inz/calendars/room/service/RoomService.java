package pkotwica.inz.calendars.room.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.room.model.Room;
import pkotwica.inz.calendars.room.repo.RoomPagingRepository;
import pkotwica.inz.calendars.room.repo.RoomRepository;
import pkotwica.inz.calendars.room.repo.RoomSpecification;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.room.web.form.RoomData;
import pkotwica.inz.calendars.user.model.User;
import pkotwica.inz.calendars.user.model.UserData;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final RoomPagingRepository roomPagingRepository;

    public RoomService(RoomRepository roomRepository, RoomPagingRepository roomPagingRepository) {
        this.roomRepository = roomRepository;
        this.roomPagingRepository = roomPagingRepository;
    }

    public List<RoomData> getAllRoomsData() {
        return roomRepository.findAll().stream().map(r -> RoomData.builder().name(r.getName()).number(r.getNumber()).build()).collect(Collectors.toList());
    }

    public Page<RoomData> getAllRoomsData(Pageable page) {
        Page<Room> roomsPage = roomPagingRepository.findAll(page);
        return createRoomDataPage(roomsPage, page);
    }

    public Page<RoomData> getAllRoomsData(Pageable page, String searchPhrase) {
        Page<Room> roomsPage = roomPagingRepository.findAll(RoomSpecification.searchInRoomData(searchPhrase), page);
        return createRoomDataPage(roomsPage, page);
    }

    public Optional<Room> getRoomByName(String name) {
        return roomRepository.findByName(name);
    }

    public boolean addNewRoom(EditRoomForm editRoomForm) {
        if (roomRepository.findRoomsByNameOrNumber(editRoomForm.getRoomName(), editRoomForm.getRoomNumber()).isEmpty()) {
            roomRepository.save(Room.builder().name(editRoomForm.getRoomName()).number(editRoomForm.getRoomNumber()).build());
            return true;
        }

        return false;
    }

    public boolean editRoom(EditRoomForm editRoomForm) {
        Optional<Room> changingRoom = roomRepository.findByName(editRoomForm.getOldName());
        List<Room> matchingRooms = roomRepository.findRoomsByNameOrNumber(editRoomForm.getRoomName(), editRoomForm.getRoomNumber());

        if (changingRoom.isPresent() && matchingRooms.stream().noneMatch(r -> r.getId() != changingRoom.get().getId())) {
           changingRoom.get().setName(editRoomForm.getRoomName());
           changingRoom.get().setNumber(editRoomForm.getRoomNumber());
           roomRepository.save(changingRoom.get());
           return true;
        }

        return false;
    }

    public boolean removeRoom(String room) {
        Optional<Room> changingRoom = roomRepository.findByName(room);

        if (changingRoom.isPresent()) {
            roomRepository.delete(changingRoom.get());
            return true;
        }

        return false;
    }

    private Page<RoomData> createRoomDataPage(Page<Room> roomsPage, Pageable page) {
        return new PageImpl<>(
                mapToRoomData(roomsPage.getContent()),
                page,
                roomsPage.getTotalElements()
        );
    }

    private List<RoomData> mapToRoomData(List<Room> rooms) {
        return rooms.stream().map(RoomData::of).collect(Collectors.toList());
    }
}
