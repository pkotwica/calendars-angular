package pkotwica.inz.calendars.event.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pkotwica.inz.calendars.event.model.EventType;

import java.util.Optional;

@Repository
public interface EventTypeRepository extends JpaRepository<EventType, Long> {
    Optional<EventType> findEventTypeById(Long id);
    Optional<EventType> findEventTypeByType(String type);
}
