import {Component, OnInit} from '@angular/core';
import {selectCalendarState, selectRoomsState} from '../../redux/selectors/calendar.selector';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetCalendarDetails, RoomData} from '../../redux/interfaces/calendar.interfaces';
import {GetRoomsCalendarData} from '../../redux/actions/calendar.actions';
import {SelectorOptionModel} from '../../shared/selector/selector-data.model';

@Component({
  selector: 'app-rooms-calendars',
  templateUrl: 'rooms-calendars.component.html'
})
export class RoomsCalendarsComponent implements OnInit {
  roomsData$ = this.state.select(selectRoomsState);
  calendarData$ = this.state.select(selectCalendarState);
  monthNumber: number;
  year: number;
  selectedRoom: string;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.monthNumber = this.route.snapshot.params['monthNumber'];
    this.year = this.route.snapshot.params['year'];
    this.selectedRoom = this.route.snapshot.queryParams['room'];
    this.getRoomsCalendarData();

    this.route.params.subscribe((params: Params) => {
      this.monthNumber = params['monthNumber'];
      this.year = params['year'];
      this.getRoomsCalendarData();
    });

    this.route.queryParams.subscribe((params: Params) => {
      this.selectedRoom = params['room'];
      this.getRoomsCalendarData();
    });
  }

  getRoomsCalendarData() {
    if (this.monthNumber > 12 || this.monthNumber < 1) {
      this.router.navigate(['/not-found/month']);
    } else {
      this.dispatchRoomsCalendarData();
    }
  }

  onChoseRoom(room: string) {
    let urlPath = '';
    if (this.monthNumber) {
      urlPath = `/${this.monthNumber}${this.year ? '/' + this.year : ''}`
    }

    this.router.navigate(
      ['/rooms-calendars' + urlPath],
      {queryParams: {room: room}}
    );
  }

  onChangeMonth(event: GetCalendarDetails) {
    this.router.navigate(['/rooms-calendars', event.monthNumber, event.year], {queryParams: {room: this.selectedRoom}});
  }

  dispatchRoomsCalendarData() {
    if (this.selectedRoom) {
      this.state.dispatch(new GetRoomsCalendarData({
        monthNumber: this.monthNumber,
        year: this.year,
        roomName: this.selectedRoom,
      }));
    }
  }

  parseSelectorData(rooms: RoomData[]) {
    const usersEmailsSelect: SelectorOptionModel[] = [];

    if (rooms) {
      rooms.forEach(room => usersEmailsSelect.push({value: room.name, name: room.number}));
    }

    return {values: usersEmailsSelect}
  }
}
