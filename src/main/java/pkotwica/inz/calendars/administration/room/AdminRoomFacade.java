package pkotwica.inz.calendars.administration.room;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.room.web.form.EditRoomForm;
import pkotwica.inz.calendars.room.web.form.RoomData;

@Service
public interface AdminRoomFacade {

    Page<RoomData> findRoomsToAdministrate(int pageNumber, int pageSize, String searchPhrase);

    boolean addNewRoom(EditRoomForm editRoomForm);

    boolean editRoom(EditRoomForm editRoomForm);

    boolean removeRoom(String roomName);
}
