import {CalendarDataState} from '../interfaces/calendar.interfaces';
import {CalendarActions, CalendarActionsTypes} from '../actions/calendar.actions';
import {initialLoadingStatus} from '../interfaces/common.interfaces';

const initialCalendarDataState: CalendarDataState = {
  status: initialLoadingStatus,
};

export const calendarDataReducer = (state: CalendarDataState = initialCalendarDataState, action: CalendarActions) => {
  switch (action.type) {
    case CalendarActionsTypes.GetMyCalendarData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case CalendarActionsTypes.GetUsersCalendarData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case CalendarActionsTypes.GetProjectsCalendarData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case CalendarActionsTypes.GetRoomsCalendarData: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case CalendarActionsTypes.GetCalendarDataSuccess: {
      return {
        ...state,
        calendarData: action.payload,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case CalendarActionsTypes.GetCalendarDataFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }
};
