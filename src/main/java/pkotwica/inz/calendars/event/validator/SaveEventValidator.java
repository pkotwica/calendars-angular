package pkotwica.inz.calendars.event.validator;

import pkotwica.inz.calendars.event.validator.constraint.SaveEventConstraint;
import pkotwica.inz.calendars.event.web.form.SaveEventForm;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SaveEventValidator implements ConstraintValidator<SaveEventConstraint, SaveEventForm> {

    @Override
    public void initialize(SaveEventConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(SaveEventForm saveEventForm, ConstraintValidatorContext constraintValidatorContext) {
        return timeIsValid(saveEventForm.getStartHour(), saveEventForm.getStartMinute()) && timeIsValid(saveEventForm.getEndHour(), saveEventForm.getEndMinute()) && startTimeIsNtoBiggerThanEnd(saveEventForm);
    }

    private boolean startTimeIsNtoBiggerThanEnd(SaveEventForm saveEventForm) {
        if (saveEventForm.getStartHour() > saveEventForm.getEndHour()) {
            return false;
        } else if (saveEventForm.getStartHour().equals(saveEventForm.getEndHour())) {
            return saveEventForm.getEndMinute() > saveEventForm.getStartMinute();
        }
        return true;
    }

    private boolean timeIsValid(Integer hour, Integer minute) {
        return hour >= 0 && hour < 24 && minute >= 0 && minute < 60;
    }
}
