import {Action} from '@ngrx/store';
import {OwnedProjectsDataState} from '../interfaces/calendar.interfaces';

export enum ProjectsActionsTypes {
  GetOwnedProjects = '[Project] get owned',
  GetOwnedProjectsSuccess = '[Project] get owned success',
  GetOwnedProjectsFailure = '[Project] get owned failure',
  GetProjectUsers = '[Project] get users',
  GetProjectUsersSuccess = '[Project] get users success',
  GetProjectUsersFailure = '[Project] get users failure',
}

export class GetOwnedProjects implements Action {
  readonly type = ProjectsActionsTypes.GetOwnedProjects;
}

export class GetOwnedProjectsSuccess implements Action {
  readonly type = ProjectsActionsTypes.GetOwnedProjectsSuccess;

  constructor(public payload: OwnedProjectsDataState) {
  }
}

export class GetOwnedProjectsFailure implements Action {
  readonly type = ProjectsActionsTypes.GetOwnedProjectsFailure;
}

export class GetProjectUsers implements Action {
  readonly type = ProjectsActionsTypes.GetProjectUsers;

  constructor(public payload: string) {
  }
}

export class GetProjectUsersSuccess implements Action {
  readonly type = ProjectsActionsTypes.GetProjectUsersSuccess;

  constructor(public payload: OwnedProjectsDataState) {
  }
}

export class GetProjectUsersFailure implements Action {
  readonly type = ProjectsActionsTypes.GetProjectUsersFailure;
}

export type ProjectsActions =
  GetOwnedProjects |
  GetOwnedProjectsSuccess |
  GetOwnedProjectsFailure |
  GetProjectUsers |
  GetProjectUsersSuccess |
  GetProjectUsersFailure;
