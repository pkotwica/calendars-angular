import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {selectCalendarState} from '../../redux/selectors/calendar.selector';
import {GetMyCalendarData} from '../../redux/actions/calendar.actions';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-my-calendar',
  templateUrl: 'my-calendar.component.html',
  styleUrls: ['my-calendar.component.scss']
})
export class MyCalendarComponent implements OnInit {
  calendarData$ = this.state.select(selectCalendarState);
  monthNumber: number;
  year: number;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.monthNumber = this.route.snapshot.params['monthNumber'];
    this.year = this.route.snapshot.params['year'];
    this.getMyCalendarData();

    this.route.params.subscribe((params: Params) => {
      this.monthNumber = params['monthNumber'];
      this.year = params['year'];
      this.getMyCalendarData();
    })
  }

  onChangeMonth(event) {
    this.router.navigate(['/my-calendar', event.monthNumber, event.year]);
  }

  getMyCalendarData() {
    if (this.monthNumber > 12 || this.monthNumber < 1) {
      this.router.navigate(['/not-found/month']);
    }
    this.dispatchMyCalendarData();
  }

  dispatchMyCalendarData() {
    this.state.dispatch(new GetMyCalendarData({monthNumber: this.monthNumber, year: this.year}));
  }
}
