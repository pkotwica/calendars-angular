import {NgModule} from '@angular/core';
import {SpinnerComponent} from './spinner/spinner.component';
import {CommonModule} from '@angular/common';
import {ModalComponent} from './modal/modal.component';
import {CalendarComponent} from './calendar/calendar.component';
import {DayComponent} from './calendar/day/day.component';
import {EventComponent} from './calendar/day/event/event.component';
import {CalendarDatePipe} from './calendar/calendar-date.pipe';
import {ManageEventModalComponent} from './calendar/manage-event/manage-event-modal.component';
import {FormsModule} from '@angular/forms';
import {ErrorPageComponent} from './error-page/error-page.component';
import {SelectorComponent} from './selector/selector.component';
import {PageableSelectorComponent} from './pageable/pageable-selector.component';
import {SearchBarComponent} from './search/search-bar.component';

@NgModule({
  declarations: [
    SpinnerComponent,
    ModalComponent,
    CalendarComponent,
    DayComponent,
    EventComponent,
    CalendarDatePipe,
    ManageEventModalComponent,
    ErrorPageComponent,
    SelectorComponent,
    PageableSelectorComponent,
    SearchBarComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    SpinnerComponent,
    CommonModule,
    ModalComponent,
    CalendarComponent,
    ErrorPageComponent,
    SelectorComponent,
    PageableSelectorComponent,
    SearchBarComponent,
  ]
})
export class SharedModule {

}
