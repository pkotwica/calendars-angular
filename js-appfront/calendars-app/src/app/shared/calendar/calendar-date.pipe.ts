import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'calendarDate'
})
export class CalendarDatePipe implements PipeTransform {
  transform(value: string): string {
    const date = new Date(value);
    let additionalZeroMinutes = date.getMinutes().toString().length === 1 ? '0' : '';
    let additionalZeroHours = date.getHours().toString().length === 1 ? '0' : '';
    return `${additionalZeroHours}${date.getHours()}:${date.getMinutes()}${additionalZeroMinutes}`;
  }
}
