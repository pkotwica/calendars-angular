package pkotwica.inz.calendars.room.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pkotwica.inz.calendars.room.service.RoomService;
import pkotwica.inz.calendars.room.web.form.RoomData;
import pkotwica.inz.calendars.user.model.UserData;
import pkotwica.inz.calendars.user.service.UserService;

import java.util.List;

import static pkotwica.inz.calendars.web.factory.GlobalsCalendarsMapping.PROTECTED_API_PREFIX;

@Controller
@RequestMapping
public class RoomController {

    @AllArgsConstructor
    @Getter
    @Setter
    static class RoomsList {
        private List<RoomData> rooms;
    }

    private final UserService userService;
    private final RoomService roomService;

    public RoomController(UserService userService, RoomService roomService) {
        this.userService = userService;
        this.roomService = roomService;
    }

    @GetMapping(PROTECTED_API_PREFIX + "/rooms-info")
    @ResponseBody
    public RoomsList getRooms() {
        return new RoomsList(this.roomService.getAllRoomsData());
    }

    @ModelAttribute(name = "userData")
    public UserData getUserData() {
        return userService.getUserData();
    }
}
