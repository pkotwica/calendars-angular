import {NgModule} from '@angular/core';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {AdminUsersComponent} from './users/admin-users.component';
import {AdminProjectsComponent} from './projects/admin-projects.component';
import {AdminRoomsComponent} from './rooms/admin-rooms.component';
import {ChangeUserDataModalComponent} from './users/change-user-data/change-user-data-modal.component';
import {RegisterUserModalComponent} from './users/register-user/register-user-modal.component';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {ChangeUserPassModalComponent} from './users/change-user-password/change-user-pass-modal.component';
import {AddRoomModalComponent} from './rooms/add-room/add-room-modal.component';
import {ChangeRoomDataModalComponent} from './rooms/change-room-data/change-room-data-modal.component';
import {AdminProjectComponent} from './projects/project/admin-project.component';
import {ChangeProjectModalComponent} from './projects/project/change-project/change-project-modal.component';
import {AddProjectModalComponent} from './projects/add-project/add-project-modal.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminUsersComponent,
    AdminProjectsComponent,
    AdminRoomsComponent,
    RegisterUserModalComponent,
    ChangeUserDataModalComponent,
    ChangeUserPassModalComponent,
    AddRoomModalComponent,
    ChangeRoomDataModalComponent,
    AddProjectModalComponent,
    AdminProjectComponent,
    ChangeProjectModalComponent,
  ],
  imports: [
    AdminRoutingModule,
    SharedModule,
    FormsModule,
  ],
  exports: []
})
export class AdminModule {
}
