package pkotwica.inz.calendars.room.repo;

import org.springframework.data.jpa.domain.Specification;
import pkotwica.inz.calendars.common.CommonSpecifications;
import pkotwica.inz.calendars.room.model.Room;

import java.util.Arrays;

public class RoomSpecification {

    private static final String NAME_FIELD = "name";
    private static final String NUMBER_FIELD = "number";

    public static Specification<Room> searchInRoomData(String phrase) {
        return CommonSpecifications.searchInListOfFields(Arrays.asList(NAME_FIELD, NUMBER_FIELD), phrase);
    }
}
