package pkotwica.inz.calendars.authorization.service;

import pkotwica.inz.calendars.authorization.model.LoginBody;
import pkotwica.inz.calendars.authorization.model.AuthToken;
import pkotwica.inz.calendars.user.model.User;

import java.util.Optional;

public interface AuthService {
    Optional<AuthToken> generateAuthToken(LoginBody loginBody);

    Optional<User> findUserByToken(String tokenCode);
}
