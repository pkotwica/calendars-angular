import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {CalendarService} from '../services/calendar.service';
import {EventActionsTypes, GetEventsTypesFailure, GetEventsTypesSuccess} from '../actions/event.actions';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {of} from 'rxjs';
import {EventsTypesDataState} from '../interfaces/calendar.interfaces';

@Injectable()
export class EventEffects {

  @Effect()
  getEventTypes$ = this.actions$.pipe(
    ofType(EventActionsTypes.GetEventsTypes),
    exhaustMap(() => this.calendarService.getEventsTypes().pipe(
      map((eventsTypes: EventsTypesDataState) => new GetEventsTypesSuccess(eventsTypes)),
      catchError(() => of(new GetEventsTypesFailure())),
    )),
  );

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private calendarService: CalendarService,
  ) {
  }
}
