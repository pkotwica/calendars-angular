package pkotwica.inz.calendars.user.validator.constraint;

import pkotwica.inz.calendars.user.validator.ChangePasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ChangePasswordValidator.class)
@Target( ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ChangePasswordConstraint {
    String message() default "Password repetition not match";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
