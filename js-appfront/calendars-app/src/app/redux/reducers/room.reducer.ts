import {RoomsDataState} from '../interfaces/calendar.interfaces';
import {initialLoadingStatus} from '../interfaces/common.interfaces';
import {RoomsActions, RoomsActionsTypes} from '../actions/room.actions';

const initialRoomsDataState: RoomsDataState = {
  status: initialLoadingStatus,
};

export const roomsDataReducer = (state: RoomsDataState = initialRoomsDataState, action: RoomsActions) => {
  switch (action.type) {
    case RoomsActionsTypes.GetRooms: {
      return {
        ...state,
        status: {
          error: false,
          loading: true,
        }
      }
    }
    case RoomsActionsTypes.GetRoomsSuccess: {
      return {
        ...state,
        rooms: action.payload.rooms,
        status: {
          error: false,
          loading: false,
        }
      }
    }
    case RoomsActionsTypes.GetRoomsFailure: {
      return {
        ...state,
        status: {
          error: true,
          loading: false,
        }
      }
    }
    default: {
      return state;
    }
  }
};
