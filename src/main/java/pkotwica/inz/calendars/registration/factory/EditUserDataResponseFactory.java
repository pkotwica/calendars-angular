package pkotwica.inz.calendars.registration.factory;

import pkotwica.inz.calendars.web.factory.CalendarsApiResponseFactory;
import pkotwica.inz.calendars.web.response.CalendarsApiResponse;

public class EditUserDataResponseFactory extends CalendarsApiResponseFactory {
    private static final String EDIT_USER_DATA_FAIL_MESSAGE = "Some data may occurs in database. Pleas try again with proper data.";
    private static final String PASSWORD_NOT_MATCH_MESSAGE = "Password not match.";
    private static final String REMOVE_USER_FAILED_MESSAGE = "Remove user failed. Please contact with support.";

    public static CalendarsApiResponse ediByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, EDIT_USER_DATA_FAIL_MESSAGE);
    }

    public static CalendarsApiResponse passByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, PASSWORD_NOT_MATCH_MESSAGE);
    }

    public static CalendarsApiResponse removeByIsSuccess(boolean isSuccess) {
        return byIsSuccess(isSuccess, REMOVE_USER_FAILED_MESSAGE);
    }
}
