import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map} from 'rxjs/operators';
import {of} from 'rxjs';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {GetRolesFailure, GetRolesSuccess, RolesActionsTypes} from '../actions/roles.actions';
import {UserService} from '../services/user.service';
import {SystemRolesDataState} from '../interfaces/user.interfaces';

@Injectable()
export class RolesEffects {

  @Effect()
  getRoles$ = this.actions$.pipe(
    ofType(RolesActionsTypes.GetRoles),
    exhaustMap(() => this.userService.getSystemRoles().pipe(
      map((roles: SystemRolesDataState) => new GetRolesSuccess(roles)),
      catchError(() => of(new GetRolesFailure())),
    )),
  );

  constructor(
    private actions$: Actions,
    private state$: Store<State>,
    private userService: UserService,
  ) {
  }
}
