import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {SharedActionsTypes} from '../actions/shared.actions';
import {switchMap} from 'rxjs/operators';
import {ResetUserData} from '../actions/user.actions';

@Injectable()
export class SharedEffects {

  @Effect()
  resetStates$ = this.actions$.pipe(
    ofType(SharedActionsTypes.ResetStates),
    switchMap(() => [
      new ResetUserData(),
    ])
  );

  constructor(
    private actions$: Actions,
  ) {
  }
}
