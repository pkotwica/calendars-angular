import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../app.redux';
import {selectUserToAdminState} from '../../../redux/selectors/admin.selector';
import {GetUsers} from '../../../redux/actions/admin.actions';
import {UserDataState} from '../../../redux/interfaces/user.interfaces';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-admin-users',
  templateUrl: 'admin-users.component.html',
  styleUrls: ['admin-users.component.scss'],
})
export class AdminUsersComponent implements OnInit {

  pageNumberParamName: string = 'page';
  searchPhraseParamName: string = 'search';
  pageNumber: number;
  searchPhrase: string;

  usersToAdministrate$ = this.state.select(selectUserToAdminState);

  isRegisterModal: boolean = false;
  isUserDataModal: boolean = false;
  isUserPassModal: boolean = false;

  selectedUser: UserDataState;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.pageNumber = this.route.snapshot.queryParams[this.pageNumberParamName];
    this.searchPhrase = this.route.snapshot.queryParams[this.searchPhraseParamName];
    this.state.dispatch(new GetUsers({page: this.pageNumber, searchPhrase: this.searchPhrase}));

    this.route.queryParams.subscribe((params: Params) => {
      this.pageNumber = params[this.pageNumberParamName];
      this.searchPhrase = params[this.searchPhraseParamName];
      this.state.dispatch(new GetUsers({page: this.pageNumber, searchPhrase: this.searchPhrase}));
    });
  }

  changeUserData(user: UserDataState) {
    this.selectedUser = user;
    this.isUserDataModal = true;
  }

  changeUserPass(user: UserDataState) {
    this.selectedUser = user;
    this.isUserPassModal = true;
  }

  onPageSelected(pageNumber: number) {
    this.router.navigate(['/admin/users'], {
      queryParams: {[this.pageNumberParamName]: pageNumber, [this.searchPhraseParamName]: this.searchPhrase}
    })
  }

  onSearchPhrase(searchPhrase: string) {
    this.router.navigate(['/admin/users'], {
      queryParams: {[this.searchPhraseParamName]: searchPhrase}
    })
  }

  onResetSearch() {
    this.router.navigate(['/admin/users'])
  }
}
