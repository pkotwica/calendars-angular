package pkotwica.inz.calendars.event.model;

public enum EventTypes {
    OFFICE, HOLIDAYS, PRIVATE, AFK, WFH, CUSTOM
}
