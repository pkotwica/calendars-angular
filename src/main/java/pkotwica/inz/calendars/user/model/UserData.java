package pkotwica.inz.calendars.user.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserData {
    public UserData(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.jobPosition = user.getJobPosition();
        this.login = user.getLogin();
        this.role = user.getRole().getName();
    }

    private final String firstName;
    private final String lastName;
    private final String email;
    private final String jobPosition;
    private final String login;
    private final String role;
}
