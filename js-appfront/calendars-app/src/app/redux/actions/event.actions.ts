import {Action} from '@ngrx/store';
import {EventsTypesDataState} from '../interfaces/calendar.interfaces';

export enum EventActionsTypes {
  GetEventsTypes = '[Event] get types',
  GetEventsTypesSuccess = '[Event] get types success',
  GetEventsTypesFailure = '[Event] get types failure',
}

export class GetEventsTypes implements Action {
  readonly type = EventActionsTypes.GetEventsTypes;
}

export class GetEventsTypesSuccess implements Action {
  readonly type = EventActionsTypes.GetEventsTypesSuccess;

  constructor(public payload: EventsTypesDataState) {
  }
}

export class GetEventsTypesFailure implements Action {
  readonly type = EventActionsTypes.GetEventsTypesFailure;
}

export type EventActions = GetEventsTypes | GetEventsTypesSuccess | GetEventsTypesFailure;
