import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../../app.redux';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {GetAdminRooms} from '../../../redux/actions/admin.actions';
import {RoomData} from '../../../redux/interfaces/calendar.interfaces';
import {selectRoomsToAdminState} from '../../../redux/selectors/admin.selector';

@Component({
  selector: 'app-admin-rooms',
  templateUrl: 'admin-rooms.component.html',
  styleUrls: ['admin-rooms.component.scss'],
})
export class AdminRoomsComponent {

  pageNumberParamName: string = 'page';
  searchPhraseParamName: string = 'search';
  pageNumber: number;
  searchPhrase: string;

  roomsToAdministrate$ = this.state.select(selectRoomsToAdminState);

  isNewRoomModal: boolean = false;
  isEditRoomModal: boolean = false;

  selectedRoom: RoomData;

  constructor(
    private state: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.pageNumber = this.route.snapshot.queryParams[this.pageNumberParamName];
    this.searchPhrase = this.route.snapshot.queryParams[this.searchPhraseParamName];
    this.state.dispatch(new GetAdminRooms({page: this.pageNumber, searchPhrase: this.searchPhrase}));

    this.route.queryParams.subscribe((params: Params) => {
      this.pageNumber = params[this.pageNumberParamName];
      this.searchPhrase = params[this.searchPhraseParamName];
      this.state.dispatch(new GetAdminRooms({page: this.pageNumber, searchPhrase: this.searchPhrase}));
    });
  }

  addNewRoom() {
    this.isNewRoomModal = true;
  }

  changeRoomData(room: RoomData) {
    this.selectedRoom = room;
    this.isEditRoomModal = true;
  }

  onPageSelected(pageNumber: number) {
    this.router.navigate(['/admin/rooms'], {
      queryParams: {[this.pageNumberParamName]: pageNumber, [this.searchPhraseParamName]: this.searchPhrase}
    })
  }

  onSearchPhrase(searchPhrase: string) {
    this.router.navigate(['/admin/rooms'], {
      queryParams: {[this.searchPhraseParamName]: searchPhrase}
    })
  }

  onResetSearch() {
    this.router.navigate(['/admin/rooms'])
  }
}
