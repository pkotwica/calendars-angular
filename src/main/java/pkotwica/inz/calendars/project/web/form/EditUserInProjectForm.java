package pkotwica.inz.calendars.project.web.form;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EditUserInProjectForm {
    private String projectName;
    private String userEmail;
}
