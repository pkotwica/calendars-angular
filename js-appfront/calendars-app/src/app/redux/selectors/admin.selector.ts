import {State} from '../../app.redux';
import {createSelector} from '@ngrx/store';
import {AdminState, ProjectInfoData} from '../interfaces/admin.interface';
import {
  PageableAdministrateProjectsData,
  PageableAdministrateRoomsData,
  PageableAdministrateUsersData
} from '../interfaces/pageable.interface';

export const selectAdmin = (globalState: State): AdminState => globalState.adminsState;

export const selectUserToAdminState = createSelector(selectAdmin, (state: AdminState): PageableAdministrateUsersData => {
  return state && state.usersToAdministrate ? state.usersToAdministrate : {
    status: {
      error: true,
      loading: false,
      errorMessage: 'PLease wait for load the data.',
    }
  }
});

export const selectRoomsToAdminState = createSelector(selectAdmin, (state: AdminState): PageableAdministrateRoomsData => {
  return state && state.roomsToAdministrate ? state.roomsToAdministrate : {
    status: {
      error: true,
      loading: false,
      errorMessage: 'PLease wait for load the data.',
    }
  }
});

export const selectProjectsToAdminState = createSelector(selectAdmin, (state: AdminState): PageableAdministrateProjectsData => {
  return state && state.projectAdministration && state.projectAdministration.projectsToAdministrate ? state.projectAdministration.projectsToAdministrate : {
    status: {
      error: true,
      loading: false,
      errorMessage: 'PLease wait for load the data.',
    }
  }
});

export const selectProjectsInfo = createSelector(selectAdmin, (state: AdminState): ProjectInfoData => {
  return state && state.projectAdministration && state.projectAdministration.projectInfoData ? state.projectAdministration.projectInfoData : {
    status: {
      error: true,
      loading: false,
      errorMessage: 'PLease wait for load the data.',
    }
  }
});
