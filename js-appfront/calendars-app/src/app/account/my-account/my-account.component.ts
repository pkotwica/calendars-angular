import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '../../app.redux';
import {selectUserDataState} from '../../redux/selectors/user.selector';
import {take} from 'rxjs/operators';
import {GetUserData} from '../../redux/actions/user.actions';

@Component({
  selector: 'app-my-account',
  templateUrl: 'my-account.component.html',
  styleUrls: ['my-account.component.scss'],
})
export class MyAccountComponent implements OnInit {

  userData$ = this.state.select(selectUserDataState);
  isChangeLoginModalOpen = false;
  isChangePassModalOpen = false;

  constructor(
    private state: Store<State>,
  ) {
  }

  ngOnInit(): void {
    this.userData$.pipe(take(1)).subscribe(userData => {
      if(!userData.email) {
        this.state.dispatch(new GetUserData());
      }
    });
  }

}
