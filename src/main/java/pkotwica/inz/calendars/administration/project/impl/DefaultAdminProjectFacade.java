package pkotwica.inz.calendars.administration.project.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pkotwica.inz.calendars.administration.project.AdminProjectFacade;
import pkotwica.inz.calendars.project.service.ProjectService;
import pkotwica.inz.calendars.project.web.form.EditProjectForm;
import pkotwica.inz.calendars.project.web.form.EditUserInProjectForm;
import pkotwica.inz.calendars.project.web.form.ProjectData;
import pkotwica.inz.calendars.project.web.form.ProjectInfo;

@Service
public class DefaultAdminProjectFacade implements AdminProjectFacade {

    private ProjectService projectService;

    public DefaultAdminProjectFacade(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public Page<ProjectData> findRoomsToAdministrate(int pageNumber, int pageSize, String searchPhrase) {
        Pageable page = PageRequest.of(pageNumber, pageSize);

        return searchPhrase != null && !searchPhrase.isEmpty() ?
                projectService.getAllProjectsData(page, searchPhrase) :
                projectService.getAllProjectsData(page);
    }

    @Override
    public ProjectInfo getProjectUsers(String projectName) {
        return ProjectInfo.of(projectName, projectService.getUsersByProject(projectName), projectService.getUsersNotInProject(projectName));
    }

    @Override
    public boolean addProject(EditProjectForm editProjectForm) {
        return projectService.addNewProject(editProjectForm);
    }

    @Override
    public boolean editProject(EditProjectForm editProjectForm) {
        return projectService.editProject(editProjectForm);
    }

    @Override
    public boolean removeProject(EditProjectForm editProjectForm) {
        return projectService.removeProject(editProjectForm);
    }

    @Override
    public boolean addUserToProject(EditUserInProjectForm editUserInProjectForm) {
        return projectService.addUserToProject(editUserInProjectForm);
    }

    @Override
    public boolean removeUserFromProject(EditUserInProjectForm editUserInProjectForm) {
        return projectService.removeUserFromProject(editUserInProjectForm);
    }
}
